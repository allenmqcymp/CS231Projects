import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.util.ArrayList;

/**
 * A class that finds the n most common words from a file of words
 *
 * it adds stuff into a bst, then after adding everything into a bst,
 * dumps all that stuff into a pqheap, which is a max heap
 * pqheap uses value comparison to compare frequencies
 * then to find the n most common words is just as trivial as calling remove n times
 */

public class FindCommonWords {

    private WordCounter wc;
    private PQHeap<KeyValuePair<String, Integer>> heap;

    public FindCommonWords() {
        wc = new WordCounter( new BSTMap<>(new StringAscending() ) );
        heap = new PQHeap<KeyValuePair<String, Integer>>( new KeyValueComparator() );
    }

    /**
     * Takes in a filename, reads the filename into a BST
     * representing keyvaluepairs of strings and integers (frequencies)
     * then dumps all the keyvalue pairs into a heap
     * @param filename
     */
    private void run(String filename) {
        // clear the wordmap
        wc.getWordmap().clear();
        wc.analyze(filename);
        for (KeyValuePair<String, Integer> thing: wc.getWordmap().entrySet()) {
            heap.add(thing);
        }
    }

    /**
     * Finds the n most common words
     * @param n
     */
    public ArrayList<KeyValuePair<String, Integer>> findMostCommon(int n) {
        ArrayList<KeyValuePair<String, Integer>> l = new ArrayList<>();
        if (heap.size() < n) {
            throw new IllegalArgumentException("the size n given is bigger than the current heap size");
        }
        for (int i = 0; i < n; i++) {
            l.add(heap.remove());
        }
        return l;
    }

    public static void main(String[] args) {
        FindCommonWords fc = new FindCommonWords();
        fc.run("./resources/reddit_comments_2008.txt");
        System.out.println("2008");
        for (KeyValuePair<String, Integer> kv : fc.findMostCommon(10)) {
            System.out.println(kv);
        }
        fc.run("./resources/reddit_comments_2012.txt");
        System.out.println("2012");
        for (KeyValuePair<String, Integer> kv : fc.findMostCommon(10)) {
            System.out.println(kv);
        }
        fc.run("./resources/reddit_comments_2015.txt");
        System.out.println("2015");
        for (KeyValuePair<String, Integer> kv : fc.findMostCommon(10)) {
            System.out.println(kv);
        }
    }

}
