import java.util.Comparator;

// More code that uses the KeyValuePair!!
// To sort by Value in to put smallest as the first removed
class KeyValueComparator implements Comparator<KeyValuePair<String, Integer>> {
    public int compare( KeyValuePair<String,Integer> i1, KeyValuePair<String,Integer> i2 ) {
        // returns negative number if i2 comes after i1 lexicographically
        return i1.getValue() - i2.getValue();
    }
}