/**
 @author allen_ma
 Template for the BSTMap classes
 Spring 2018
 CS 231 Project 6

 Note that the size variable increments by 1 everytime a new node is called
 */
import java.util.ArrayList;
import java.util.Comparator;

public class BSTMap<K, V> implements MapSet<K, V> {

    protected TNode<K, V> root;
    protected Comparator<K> comparator;
    protected int size;

    // constructor: takes in a Comparator object
    public BSTMap( Comparator<K> comp ) {
        // initialize fields here
        root = null;
        comparator = comp;
        size = 0;
    }

    // Put the key-value pair into the BSTMap
    // Returns the old value or null if a new key was added
    public V put( K key, V value ) {
        if (root == null) {
            root = new TNode<K, V>(key, value, null, null);
            size++;
            return null;
        }
        else {
            return root.put(key, value, comparator);
        }
    }

    // gets the value at the specified key or null
    public V get( K key ) {
        // check for and handle the special case
        if (root == null) return null;
        else {
            return root.get(key, comparator);
        }
    }

    /**
     * Checks if key is found in the bst
     * @param key
     * @return whether or not the key exists
     */
    public boolean containsKey( K key ) {
        if (root == null) return false;
        else {
            return root.containsKey(key, comparator);
        }
    }

    public int getHeight() {
        if (root == null) {
            return -1;
        }
        return root.getHeight();
    }

    /**
     * @return returns arraylist of all the keys, in pre-order traversal
     */
    public ArrayList<K> keySet() {
        ArrayList<K> l = new ArrayList<K>();
        if (root == null) {
            return l;
        }
        else {
            l.add(root.getNode().getKey());
            traverseKeySet(root.getLeft(), l);
            traverseKeySet(root.getRight(), l);
            return l;
        }
    }

    private void traverseKeySet(TNode<K, V> node, ArrayList<K> l) {
        if (node != null) {
            l.add(node.getNode().getKey());
            traverseKeySet(node.getLeft(), l);
            traverseKeySet(node.getRight(), l);
        }
    }

    /**
     * @return return arraylist of values in the same order as keyset
     * a pre-order traversal
     */
    public ArrayList<V> values() {
        ArrayList<V> l = new ArrayList<V>();
        if (root == null) return l;
        else {
            l.add(root.getNode().getValue());
            traverseValues(root.getLeft(), l);
            traverseValues(root.getRight(), l);
            return l;
        }
    }

    private void traverseValues(TNode<K, V> node, ArrayList<V> l) {
        if (node != null) {
            l.add(node.getNode().getValue());
            traverseValues(node.getLeft(), l);
            traverseValues(node.getRight(), l);
        }
    }

    /**
     * @return return arraylist of keyvalue pairs using a pre-order traversal
     */
    public ArrayList<KeyValuePair<K,V>> entrySet() {
        ArrayList<KeyValuePair<K, V>> l = new ArrayList<>();
        if (root == null) {
            return l;
        }
        else {
            l.add(root.getNode());
            traverseEntrySet(root.getLeft(), l);
            traverseEntrySet(root.getRight(), l);
            return l;
        }
    }

    /**
     * traverseEntryset for entrySet
     * a pre-order traversal
     * @return
     */
    private void traverseEntrySet(TNode<K, V> node, ArrayList<KeyValuePair<K,V>> l) {
        if (node != null) {
            l.add(node.getNode());
            traverseEntrySet(node.getLeft(), l);
            traverseEntrySet(node.getRight(), l);
        }
    }

    /**
     * @return Return the number of key-value pairs in the map
     */
    public int size() {
        // traverse the tree
        if (root == null) return 0;
        else {
            int sz = sizeNode(root);
            return sz;
        }
    }

    /**
     * Recursively traverse a subtree
     * @param node
     * @return
     */
    private int sizeNode(TNode<K, V> node) {
        if (node == null) return 0;
        else {
            return (1 + sizeNode(node.getLeft()) + sizeNode(node.getRight()) );
        }
    }

    //


    /**
     * Getters and setters
     */

    public Comparator<K> getComparator() {
        return comparator;
    }

    /**
     * Clears the entire bst by removing all references
     * Is setting root to null sufficient for the garbage collector to know?
     */
    public void clear() {
        size = 0;
        root = null;
    }

    /**
     *     "john:0"
     *  ______|_______
     * |              |
     * "j:6"   "doris:8"
     *
     *
     * eg. ( twenty:20 ( ten:10 ( eleven:11  null ( five:5  null ( six:6  null  null ))) null ) null )
     * can be reconstructed into a tree
     *
     * returns (john:0 (j:6) (doris:8))
     */
    private String toStringNode(TNode<K, V> node) {
        if (node == null) return " null ";
        else {
            String pairStr = toStringPair(node.getNode().getKey(), node.getNode().getValue());
            return "(" + pairStr + toStringNode(node.getLeft()) +  toStringNode(node.getRight()) + ")";
        }
    }

    /**
     * Remove method
     */
    public void remove(K key) {
        if (root == null) throw new IllegalArgumentException("cannot remove from null root");
        if (comparator.compare(key, root.getNode().getKey()) == 0) {
            root = root.removeFoundNode(root, comparator);
        }
        else if (comparator.compare(key, root.getNode().getKey()) < 0)
            root.remove(key, root.getLeft(), comparator);
        else
            root.remove(key, root.getRight(), comparator);
    }


    /**
     * Returns a legitimate although not-human-friendly representation of the tree.
     * Use a pair of parentheses to represent a subtree
     * @return
     */
    @Override
    public String toString() {
        if (root == null) return null;
        else return toStringNode(root);
    }

    private String toStringPair(K k, V v) {
        return " " + k.toString() + ":" + v.toString() + " ";
    }


    /**
     * Gets root - required to print the tree
     * @return
     */
    public TNode<K, V> getRoot() {
        return root;
    }

//    // test function
//    public static void main( String[] argv ) {
//
//        // create a BSTMap
//        BSTMap<String, Integer> bst = new BSTMap<String, Integer>( new StringAscending() );
//
//        bst.put( "g", 20 );
//        bst.put("f", 12);
//        bst.put("k", 1);
//        bst.put("j", 2);
//        bst.put("m", 3);
//        bst.put("e", 2);
//        bst.put("d", 4);
//        bst.put("c", 1);
//
//        System.out.println("Size of bst is " + bst.size());
//
//        System.out.println(bst);
//
//        for (KeyValuePair<String, Integer> kv: bst.entrySet()) {
//            System.out.println(kv);
//        }
//
//        bst.remove("j");
//
//        System.out.println("After removing");
//
//        System.out.println(bst);
//
//        for (KeyValuePair<String, Integer> kv: bst.entrySet()) {
//            System.out.println(kv);
//        }
//
//    }

}
