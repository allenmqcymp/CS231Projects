import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.Hashtable;

public class FrequencyAnalyze {

    private static final String BASEPATH = "./resources/reddit_comments_";
    WordCounter wc = new WordCounter(new BSTMap<>(new StringAscending()) );

    private void runAnalysis(int start, int end, String[] args) {
        Hashtable<String, ArrayList<Double>> hasht = new Hashtable<>();
        for (String word : args) {
            hasht.put(word, new ArrayList<Double>());
        }
        for (int year = start; year <= end; year++) {
            // construct a word map
            wc.analyze(BASEPATH + Integer.toString(year) + ".txt");
            for (String word : args) {
                if (!wc.getWordmap().containsKey(word)) {
                    hasht.get(word).add(0.0);
                }
                else {
                    hasht.get(word).add(wc.getFrequency(word));
                }
            }
        }
        // print the results out in csv format
        for (String k  : hasht.keySet()) {
            System.out.println(k);
            System.out.println(hasht.get(k).toString());
        }

    }

    public WordCounter getWc() {
        return wc;
    }

    public void startup(String[] args) {

        int startDate = 2008;
        int endDate = 2015;

        Options options = new Options();
        options.addOption("start", true, "start date");
        options.addOption("end", true, "end date");
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("start")) startDate = Integer.parseInt(cmd.getOptionValue("start"));
            else startDate = 2008;

            if (cmd.hasOption("end")) endDate = Integer.parseInt(cmd.getOptionValue("end"));
            else endDate = 2015;

        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        // args is the list of words
        runAnalysis(startDate, endDate, args);
    }



    public static void main(String[] args) {
        FrequencyAnalyze freqA = new FrequencyAnalyze();

        WordCounter w = freqA.getWc();
        freqA.getWc().analyze("./resources/title_comments_serial.txt");
        int bron_count = w.getCount("lebron");
        System.out.println("Lebron," + bron_count);
        System.out.println("Harden,"+w.getCount("harden"));
        System.out.println("Westbrook," + w.getCount("westbrook"));
        System.out.println("Klay,"+w.getCount("klay"));
        System.out.println("Embiid,"+w.getCount("embiid"));

    }
}
