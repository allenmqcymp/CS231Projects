# parse the txt file
import ast
import csv

class Wrangler:


    def __init__(self, filename, output_filename, means_filename):
        self.years_list = []
        self.all_data_list = []
        self.means = []
        self.filename = filename
        self.output_filename = output_filename
        self.means_filename = means_filename


    def parse(self):

        parse = False

        with open(self.filename) as f:
            f = f.readlines()

        for line in f:

            if line.strip() == "bst_results":
                parse = False

            if parse:
                # write to "hashmap_results.csv"
                data = line.split("=")
                year = data[0]
                self.years_list.append(year)
                # split by comma to obtain list
                times = ast.literal_eval(data[1])
                self.all_data_list.append(times)

            if line.strip() == "hashmap_results":
                parse = True

        with open(self.output_filename, "w") as c:
            csvwriter = csv.writer(c)
            csvwriter.writerow(self.years_list)
            col_list = zip(*self.all_data_list)
            for l in col_list:
                csvwriter.writerow(l)


    def process(self):
        for year_data in self.all_data_list:
            year_data = self.drop_lgsmall(year_data)
            print(year_data)
            self.means.append(sum(year_data) // len(year_data))

        with open(self.means_filename, "w") as m:
            csv_means_writer = csv.writer(m)
            zip_means = zip(self.years_list, self.means)
            for m in zip_means:
                csv_means_writer.writerow(m)

        # write means to a csv file
        print("done processing")

    def drop_lgsmall(self, l):
        l.remove(min(l))
        l.remove(max(l))
        return l



if __name__ == "__main__":
    wr = Wrangler("results.txt", "hashmap_results.csv", "means_hashmap_results.csv")
    wr.parse()
    wr.process()
