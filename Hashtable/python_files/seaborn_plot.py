import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd



def plot_line_from_file(filename):
    data_bst = np.genfromtxt(filename, delimiter=',', names=["year", "runtime"])
    plt.plot(data_bst['year'], data_bst['runtime'])


def main():
    plot_line_from_file("means_bst_results.csv")
    plot_line_from_file("means_hashmap_results.csv")
    plt.title("Mean runtime of Reddit Comments Mapping")
    plt.ylabel("Runtime/ms")
    plt.xlabel("Year of Reddit Comment File - increasing in size")
    plt.legend(["BST", "Hashmap"])
    plt.show()


if __name__ == "__main__":
    main()

