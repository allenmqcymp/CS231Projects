/**
 * Utility class for running files
 */
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to run tests on reddit comments
 */

public class WordRunner {

    // word counter classes
    private static WordCounter wc_bst = new WordCounter("BSTMap");

    private static WordCounter wc_hashmap = new WordCounter("Hashmap");

    // these are just convenience hashmaps for storing result data
    private HashMap<Integer, ArrayList<Long>> results_bstmap = new HashMap<>();

    private HashMap<Integer, ArrayList<Long>> results_hashmap = new HashMap<>();

    private static final int num_runs = 5;

    public HashMap<Integer, ArrayList<Long>> getResults_bstmap() {
        return results_bstmap;
    }

    public HashMap<Integer, ArrayList<Long>> getResults_hashmap() {
        return results_hashmap;
    }

    /**
     * Runs a single year 5 times writing the results in a java inbuilt hashmap (just to store the values)
     * @param year
     */
    public void run_year(int year) {
        wc_hashmap.getWordmap().clear();
        wc_bst.getWordmap().clear();

        String filename = "./resources/reddit_comments_" + year + ".txt";

        // warm-up both
        wc_bst.betterAnalyze(filename);
        wc_hashmap.betterAnalyze(filename);
        wc_bst.getWordmap().clear();
        wc_hashmap.getWordmap().clear();

        System.out.println("Finished warming up for year " + year);

        for (int i = 0; i < num_runs; i++) {

            System.out.println("Iteration number " + i);

            Timer.start();
            wc_bst.betterAnalyze(filename);
            Timer.stop();

            ArrayList<Long> value_at_year = results_bstmap.get(year);
            if (value_at_year == null) {
                value_at_year = new ArrayList<Long>();
                value_at_year.add(Timer.getRuntime());
                results_bstmap.put(year, value_at_year);
            }
            else {
                results_bstmap.get(year).add(Timer.getRuntime());
            }

            Timer.start();
            wc_hashmap.betterAnalyze(filename);
            Timer.stop();

            ArrayList<Long> bst_value_year = results_hashmap.get(year);
            if (bst_value_year == null) {
                bst_value_year = new ArrayList<Long>();
                bst_value_year.add(Timer.getRuntime());
                results_hashmap.put(year, bst_value_year);
            }
            else {
                results_hashmap.get(year).add(Timer.getRuntime());
            }

        }
    }

    /**
     * Runs 5 tests for years 2008 - 2015
     * Beware - can take quite a long time
     * @param filename
     * @throws IOException
     */
    private void runAll(String filename) throws IOException{
        FileWriter fw = new FileWriter(filename);
        WordRunner runner = new WordRunner();

        int YEAR_START = 2008;
        int YEAR_END = 2015;
        for (int year = YEAR_START; year <= YEAR_END; year++) {
            System.out.println("Running year " + year);
            runner.run_year(year);
        }

        fw.write("hashmap results");
        fw.write("\n");
        HashMap<Integer, ArrayList<Long>> map = runner.getResults_hashmap();
        for (Map.Entry<Integer, ArrayList<Long>> res : map.entrySet()) {
            fw.write(res.toString());
            fw.write("\n");
        }

        fw.write("bst results");
        fw.write("\n");
        HashMap<Integer, ArrayList<Long>> map2 = runner.getResults_bstmap();
        for (Map.Entry<Integer, ArrayList<Long>> res : map2.entrySet()) {
            fw.write(res.toString());
            fw.write("\n");
        }

        System.out.println("done");

        fw.close();

    }

    public static void main(String[] args) throws IOException {
        WordRunner runner = new WordRunner();

        runner.runAll("./outputfiles/results_test.txt");
    }

}

