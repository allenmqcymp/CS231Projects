import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Chaining hashmap implementation
 * Allen Ma
 * April 2018
 * @param <K> Key type
 * @param <V> Value type
 */

public class Hashmap<K,V> implements MapSet<K,V> {

    // init size is kind of arbitrary

    private int size;
    private static int INIT_SIZE = 1000;
    private Object[] hash_table;
    private Comparator<K> comp;
    private int collisions;

    public Hashmap(Comparator<K> comp) {
        this.comp = comp;
        hash_table = new Object[INIT_SIZE];
    }

    public V put( K new_key, V new_value ) {
        int code = new_key.hashCode();
        int index = Math.abs(code % hash_table.length);

        if (hash_table[index] == null) {
            hash_table[index] = new BSTMap<K, V>(comp);
            BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[index];
            map_index.put(new_key, new_value);
            return null;
        }
        else {
            // collision occurs when bst already present
            // if remove functionality added then this needs to be refactored
            collisions++;
            BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[index];
            return map_index.put(new_key, new_value);
        }
    }

    // Returns true if the map contains a key-value pair with the given key
    public boolean containsKey( K key ) {
        int code = key.hashCode();
        int index = Math.abs(code % hash_table.length);
        if (hash_table[index] != null) {
            BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[index];
            return map_index.containsKey(key);
        }
        return false;
    }

    // Returns the value associated with the given key.
    // If that key is not in the map, then it returns null.
    public V get( K key ) {
        int code = key.hashCode();
        int index = Math.abs(code % hash_table.length);
        if (hash_table[index] != null) {
            BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[index];
            return map_index.get(key);
        }
        return null;
    }

    // Returns an ArrayList of all the keys in the map. There is no
    // defined order for the keys.
    public ArrayList<K> keySet() {
        ArrayList<K> keys = new ArrayList<K>();
        for (int i = 0; i < hash_table.length; i++) {
            if (hash_table[i] != null) {
                BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[i];
                for (K key : map_index.keySet()) {
                    keys.add(key);
                }
            }
        }
        return keys;
    }

    // Returns an ArrayList of all the values in the map. These should
    // be in the same order as the keySet.
    public ArrayList<V> values() {
        ArrayList<V> values = new ArrayList<V>();
        for (int i = 0; i < hash_table.length; i++) {
            if (hash_table[i] != null) {
                BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[i];
                for (V value : map_index.values()) {
                    values.add(value);
                }
            }
        }
        return values;
    }

    // return an ArrayList of pairs. There is no defined order for the
    // keys, and they do not need to match keySet or values (but they can).
    public ArrayList<KeyValuePair<K,V>> entrySet() {
        ArrayList<KeyValuePair<K, V>> pairs = new ArrayList<KeyValuePair<K, V>>();
        for (int i = 0; i < hash_table.length; i++) {
            if (hash_table[i] != null) {
                BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[i];
                for (KeyValuePair<K, V> pair : map_index.entrySet()) {
                    pairs.add(pair);
                }
            }
        }
        return pairs;
    }

    // Returns the number of key-value pairs in the map.
    public int size() {
        // inefficient but...
        int acc_size = 0;
        for (int i = 0; i < hash_table.length; i++) {
            if (hash_table[i] != null) {
                BSTMap<K, V> map_index = (BSTMap<K, V>) hash_table[i];
                acc_size += map_index.size();
            }
        }
        return acc_size;
    }

    /**
     * Get the number of collisions
     * @return
     */
    public int getCollisions() {
        return collisions;
    }

    // removes all mappings from this MapSet
    public void clear() {
        Arrays.fill(hash_table, null);
    }

    /**
     * Test function
     * @param args
     */
    public static void main(String[] args) {
        Comparator<String> comp = new StringAscending();
        Hashmap<String, Integer> hr = new Hashmap<String, Integer>(comp);

        hr.put("harry", 1);
        hr.put("ron", 2);
        hr.put("john", 3);
        hr.put("ron", 4);
        hr.put("rick", 8);

        System.out.println(hr.size());

        for (KeyValuePair<String, Integer> pair : hr.entrySet()) {
            System.out.println(pair);
        }

    }
}
