import java.util.Comparator;

class StringAscending implements Comparator<String> {
    public int compare( String a, String b ) {
        return a.compareTo(b);
    }
}