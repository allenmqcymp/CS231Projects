/**
 * Private inner class that represents a node
 * May have left and right children
 * node field is a key, value pair
 */
import java.util.Comparator;

public class TNode<K, V> {

    private TNode<K, V> left;
    private TNode<K, V> right;
    private KeyValuePair<K, V> node;
    private TNode<K, V> parent;

    // definition of colors for llrb
    // don't want to interfere with boolean truthy values, so
    // random magic values used - not good :(
    // oh wait, java is statically typed...
    private static final int RED = 5;
    private static final int BLACK = 10;

    private int color;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    // constructor, given a key and a value
    // and left and right subtrees, initialize the current node
    public TNode( K k, V v, TNode<K, V> l, TNode<K, V> r) {
        node = new KeyValuePair<K, V>(k, v);
        left = l;
        right = r;
    }



    public TNode( K k, V v, TNode l, TNode<K, V> r, int color) {
        node = new KeyValuePair<K, V>(k, v);
        left = l;
        right = r;
        this.parent = parent;
        this.color = color;
    }

    // get and set for parent


    public TNode<K, V> getParent() {
        return parent;
    }

    public void setParent(TNode<K, V> parent) {
        this.parent = parent;
    }

    // Takes in a key, a value, and a comparator and inserts the TNode
    // Returns the old value of the node, if replaced, or null if inserted
    public V put( K key, V value, Comparator<K> comp ) {
        if (node == null) {
            return null;
        }
        if (comp.compare(key, node.getKey()) == 0) {
            V returnVal = node.getValue();
            node.setValue(value);
            return returnVal;
        }
        else if (comp.compare(key, node.getKey()) < 0) {
            if (left == null) {
                left = new TNode<K, V>(key, value, null, null);
                return null;
            }
            return left.put(key, value, comp);
        }
        else {
            if (right == null) {
                right = new TNode<K, V>(key, value, null, null);
                return null;
            }
            return right.put(key, value, comp);
        }
    }


    // Takes in a key and a comparator
    // Returns the value associated with the key or null
    public V get( K key, Comparator<K> comp ) {
        int compResult = comp.compare(key, node.getKey());
        if (compResult == 0) {
            return node.getValue();
        }
        else if (compResult < 0) { // key is smaller than node's key
            if (left == null) return null;
            else return left.get(key, comp);
        }
        else {
            if (right == null) return null;
            else return right.get(key, comp);
        }
    }


    /**
     * Note that
     * @param key
     * @param comp
     * @return
     */
    public TNode<K, V> remove(K key, TNode<K, V> currNode, Comparator<K> comp) {

        if (currNode == null) return null;
        else {
            int comp_result = comp.compare(key, currNode.getNode().getKey());
            if (comp_result == 0) {
                currNode = removeFoundNode(currNode, comp);
            }
            else if (comp_result < 0) {
                currNode.left = remove(key, currNode.left, comp);
            }
            else {
                currNode.right = remove(key, currNode.right, comp);
            }
            return currNode;
        }
    }

    public TNode<K, V> removeFoundNode(TNode<K, V> node, Comparator<K> comp) {
        if (node.right == null && node.left == null) {
            return null;
        }
        if (node.right == null) {
            return node.left;
        }
        else if (node.left == null) {
            return node.right;
        }
        else {
            // get the biggest element of the left subtree
            TNode<K, V> temp = node;
            KeyValuePair<K, V> newNode = getLeftLargest(temp.left);
            node.setNode(newNode);
            node.right = temp.right;
            node.left = remove(newNode.getKey(), temp.left, comp);
            return node;
        }
    }

    private KeyValuePair<K, V> getLeftLargest(TNode<K, V> lsubtree) {
        if (lsubtree.getRight() == null) {
            return lsubtree.getNode();
        }
        return getLeftLargest(lsubtree.getRight());
    }


    public KeyValuePair<K, V> getNode() {
        return node;
    }

    public TNode<K, V> getLeft() {
        return left;
    }

    public TNode<K, V> getRight() {
        return right;
    }

    public void setLeft(TNode<K, V> left) {
        this.left = left;
    }

    public void setRight(TNode<K, V> right) {
        this.right = right;
    }

    public void setNode(KeyValuePair<K, V> node) {
        this.node = node;
    }

    public boolean containsKey(K key, Comparator<K> comp) {
        int compResult = comp.compare(key, node.getKey());
        if (compResult == 0) return true;
        else if (compResult < 0) {
            if (left == null) return false;
            else return left.containsKey(key, comp);
        }
        else {
            if (right == null) return false;
            else return right.containsKey(key, comp);
        }
    }
}