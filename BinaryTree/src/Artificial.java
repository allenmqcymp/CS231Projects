public class Artificial<K, V> {

    private TNode<K, V> node;
    private V val;

    public TNode<K, V> getNode() {
        return node;
    }

    public V getVal() {
        return val;
    }

    public Artificial(TNode<K, V> node, V val) {
        this.node = node;
        this.val = val;
    }
}
