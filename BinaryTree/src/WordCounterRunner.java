import java.io.IOException;
import java.util.Comparator;

public class WordCounterRunner {

    private static Comparator<String> comp = new StringAscending();
    private static WordCounter wc = new WordCounter(new BSTMap<String, Integer>(comp) );

    public WordCounterRunner() {


    }

    static String[] comments = {
            "../resources/reddit_comments_2015.txt",
    };

    public static void test(String filename, WordCounter wc) throws IOException {

        System.out.println("*** Generating word frequency map for " + filename);

        Timer.start();
        wc.analyze(filename);
        Timer.stop();

        // name the file:
        // filename_count.txt
        // strip the .txt off
        String fileName = filename.substring(0, filename.lastIndexOf('.')) + "_count.txt";

        System.out.println("Analysis took " + Timer.getRuntime());
        System.out.println("Unique wordcount is " + wc.getWordmap().size());
        System.out.println("*** Writing results to " + fileName);

        wc.writeWordCountFile(fileName);

        // clear the word counter
        wc.getWordmap().clear();
    }


    public static void main(String[] args) throws IOException {
        for (int i = 0; i < comments.length; i++) {
            test(comments[i], wc);
        }
    }
}
