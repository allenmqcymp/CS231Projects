import java.io.FileWriter;
import java.util.Comparator;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;



public class WordCounter {

    private BSTMap<String, Integer> wordmap;
    private int totalCount;

    public WordCounter(BSTMap<String, Integer> map) {
        wordmap = map;
        totalCount = 0;
    }

    /**
     * getters and setters
     */
    public BSTMap<String, Integer> getWordmap() {
        return wordmap;
    }

    /**
     * generates the word counts from a file of words
     * @param filename
     * @return
     */
    public boolean analyze(String filename) {

        try {
            FileReader fr = new FileReader(filename);
            BufferedReader bfr = new BufferedReader(fr);

            while (true) {
                String res = bfr.readLine();
                if (res == null) break;
                String[] words = res.split("[^a-zA-Z0-9']");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i].trim().toLowerCase();
                    // words cannot have length 0
                    if (word.length() == 0) continue;
                    else {
                        if (!wordmap.containsKey(word)) {
                            wordmap.put(word, 1);
                            totalCount += 1;
                        }
                        else {
                            wordmap.put(word, wordmap.get(word) + 1);
                            totalCount += 1;
                        }
                    }
                }
            }
            bfr.close();
            fr.close();
            return true;
        }
        catch(FileNotFoundException ex) {
            System.out.println("read():: unable to open file " + filename );
        }
        catch(IOException ex) {
            System.out.println("read():: error reading file " + filename);
        }
        return false;

    }

    // slightly optimized version of analyze - see extension 2
    public boolean betterAnalyze(String filename) {

        try {
            FileReader fr = new FileReader(filename);
            BufferedReader bfr = new BufferedReader(fr);

            while (true) {
                String res = bfr.readLine();
                if (res == null) break;
                String[] words = res.split("[^a-zA-Z0-9']");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i].trim().toLowerCase();
                    // words cannot have length 0
                    if (word.length() == 0) continue;
                    else {
                        Integer tentative = wordmap.get(word);
                        if (tentative != null) {
                            wordmap.put(word, tentative +1);
                        }
                        else {
                            wordmap.put(word, 1);
                        }
                    }
                }
            }
            bfr.close();
            fr.close();
            return true;
        }
        catch(FileNotFoundException ex) {
            System.out.println("read():: unable to open file " + filename );
        }
        catch(IOException ex) {
            System.out.println("read():: error reading file " + filename);
        }
        return false;

    }

    /**
     * Get the total word count of every word
     * @return
     */
    public int getTotalWordCount() {
        return totalCount;
    }

    /**
     * Get the count of a particular word of the tree
     * @param word
     * @return
     */
    public int getCount( String word ) {
        return wordmap.get(word);
    }

    /**
     * get the count of a particular word but divide it by the totalWordCount
     * @param word
     * @return
     */
    public double getFrequency( String word ) {
        return (float) wordmap.get(word) / totalCount;
    }


    public void writeWordCountFile( String filename ) throws IOException {
        FileWriter fw = new FileWriter(filename);

        // write totalCount
        fw.write("totalWordCount : " + totalCount + "\n");

        // convert the bst map to arraylist
        ArrayList<KeyValuePair<String, Integer>> wordmapList = wordmap.entrySet();

        for (KeyValuePair<String, Integer> pair: wordmapList) {
            fw.write(pair.toString() + "\n");
        }
        fw.close();

    }

    public void readWordCountFile( String filename ) {
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader bfr = new BufferedReader(fr);

            int count = 0;

            while (true) {

                String res = bfr.readLine();

                // skip the first line
                count++;
                if (count == 1) {
                    continue;
                }

                if (res == null) break;
                else {
                    String[] pair = res.split("\\s+");
                    if (!wordmap.containsKey(pair[0])) {
                        wordmap.put(pair[0], Integer.parseInt(pair[1]));
                    }
                }
            }

            bfr.close();
            fr.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("read():: unable to open file " + filename );
        }
        catch(IOException ex) {
            System.out.println("read():: error reading file " + filename);
        }
    }



    public static void main(String[] args) throws IOException {
        Comparator<String> comp = new StringAscending();
        WordCounter wc = new WordCounter(new BSTMap<String, Integer>(comp) );

        // run the analyze function
        wc.analyze("./resources/counttest.txt");
        wc.getCount("golang");

    }

}


