/**
 * Utility timer class
 */

public class Timer
{
    private static long startTime;
    private static long endTime;

    public static void start() {
        startTime = System.currentTimeMillis();
    }

    public static void stop() {
        endTime = System.currentTimeMillis();
    }

    /**
     * @return the milliseconds between the last call of start and stop
     */
    public static long getRuntime() {
        return (endTime - startTime);
    }
}