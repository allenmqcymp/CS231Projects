
/**
 * Class to hold the val
 * @param <Key> Key
 * @param <Value> Value
 *
 * @author allen_ma
 */
public class KeyValuePair<Key, Value> {

    Key key;
    Value value;

    /**
     * Constructor
     * @param k key
     * @param v value
     */
    public KeyValuePair(Key k, Value v) {
        key = k;
        value = v;
    }

    public KeyValuePair() {
        key = null;
        value = null;
    }

    public Key getKey() {
        return key;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return key + " " + value;
    }
//
//    public static void main(String[] args) {
//        KeyValuePair<String, Integer> john = new KeyValuePair<String, Integer>("John", 3);
//        KeyValuePair<String, Integer> schu = new KeyValuePair<String, Integer>("Schu", 4);
//
//        System.out.println(john.toString());
//        System.out.println(schu.toString());
//        System.out.println(john.getValue());
//        john.setValue(5);
//        System.out.println(john.toString());
//        System.out.println(schu.getKey());
//        System.out.println(schu.getValue());
//    }
}
