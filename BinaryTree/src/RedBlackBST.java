import java.util.ArrayList;
import java.util.Comparator;

/**
 * Left leaning red black tree implementation
 * Idea taken from Robert Segdewick and Kevin Wayne
 * Introduction to Algorithms 4th edition, Section 3.3, Self-Balancing Binary Trees
 * @author allenma
 */

/**
 * Note: the put method uses internal rotations to self-balance
 * The other methods (remove not implemented) work exactly the same way as in a normal BST
 * Except, it guarantees logarithmic performance (runtime)
 * @param <K>
 * @param <V>
 */

public class RedBlackBST<K, V> extends BSTMap<K, V> implements MapSet<K, V>  {


//    private TNode<K, V> root;
//    private Comparator<K> comparator;
//    private int size;

    private static final int RED = 5;
    private static final int BLACK = 10;

    public RedBlackBST(Comparator<K> comp) {
        super(comp);
    }

    private boolean isRed(TNode<K, V> node) {
        if (node == null) return false;
        return node.getColor() == RED;
    }

    private TNode<K, V> rotateLeft(TNode<K, V> node) {
        TNode<K, V> x = node.getRight();
        node.setRight(x.getLeft());
        x.setLeft(node);
        x.setColor(node.getColor());
        node.setColor(RED);
        return x;
    }

    private TNode<K, V> rotateRight(TNode<K, V> node) {
        TNode<K, V> x = node.getLeft();
        node.setLeft(x.getRight());
        x.setRight(node);
        x.setColor(node.getColor());
        node.setColor(RED);
        return x;
    }

    private void flipColors(TNode<K, V> node) {
        node.setColor(RED);
        node.getLeft().setColor(BLACK);
        node.getRight().setColor(BLACK);
    }

    // adds or updates a key-value pair
    // If there is already a pair with new_key in the map, then update
    // the pair's value to new_value.
    // If there is not already a pair with new_key, then
    // add pair with new_key and new_value.
    // returns the old value or null if no old value existed
    public V put( K key, V value ) {
        Artificial<K, V> ar = put(root, key, value);
        root = ar.getNode();
        root.setColor(BLACK);
        return ar.getVal();
    }

    // overload put method
    private Artificial<K, V> put(TNode<K, V> node, K key, V value) {
        if (node == null) {
            TNode<K, V> newNode = new TNode<K, V>(key, value, null, null, RED);
            return new Artificial<K, V>(newNode, null);
        }
        V old_val = null;

        int comp_result = comparator.compare(key, node.getNode().getKey());
        if (comp_result < 0) node.setLeft(put(node.getLeft(), key, value).getNode());
        else if (comp_result > 0) node.setRight(put(node.getRight(), key, value).getNode());
        else if (comp_result == 0) {
            old_val = node.getNode().getValue();
            node.setNode(new KeyValuePair<K, V>(key, value));
        }
        else {
            throw new IllegalStateException("cannot find right branch to insert");
        }

        if (isRed(node.getRight()) && !isRed(node.getLeft())) node = rotateLeft(node);
        if (isRed(node.getLeft()) && isRed(node.getLeft().getLeft())) node = rotateRight(node);
        if (isRed(node.getLeft()) && isRed(node.getRight())) flipColors(node);

        return new Artificial<K, V>(node, old_val);
    }

    public static void main(String[] args) {
        RedBlackBST<String, Integer> rb = new RedBlackBST<String, Integer>(new StringAscending());

        rb.put("lol", 2);

        System.out.println(rb.toString());
    }

}
