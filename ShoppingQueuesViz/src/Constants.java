
public final class Constants {
    private Constants() {

    }

    // CONSTANTS
    public static final int numCheckoutLines = 10;

    public static final int CASHIER_SIZE = 10;
    public static final int PADDING = 10;
    public static final int WIDTH = numCheckoutLines * 50;

    // 50 extra space for each cashier line
    public static final int defaultCustomers = 20;

    public static final int HEIGHT = 500;


    // CUSTOMER REPRESENTATION
    public static final int CUSTOMER_RADIUS = 4;

    public static final int CHECKOUT_OFFSET = 10;


    public static void main(String[] args) {

    }
}
