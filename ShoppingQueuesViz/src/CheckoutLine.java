/**
 * Checkoutline is a wrapper class for a  queue (node based DLL queue)
 */
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Circle;

import java.lang.Thread;


public class CheckoutLine{

    private Customer currentCustomer;
    private MyQueue<Customer> checkoutLine;

    private Node view;

    public CheckoutLine() {
        checkoutLine = new MyQueue<Customer>();
        currentCustomer = null;
    }

    public CheckoutLine(Node view) {
        checkoutLine = new MyQueue<Customer>();
        currentCustomer = null;
        this.view = view;
    }

    public MyQueue<Customer> getLine() {
        return checkoutLine;
    }

    /**
     * Get the next customer in the queue
     */
    private void getNextCustomer() {
        currentCustomer = checkoutLine.peek();
    }

    /**
     * This method should be called after initializing the customer queue
     * The queue will be null at first, so we have to set currentCustomer manually
     * after we create at least one customer
     */
    public void assignCurrentCustomer(Customer c) {
        currentCustomer = c;
    }

    /**
     * Called from simulation
     * In one timestep, either:
     *  do nothing if the line is empty
     *  or, checkout one item from a customer
     *  when the customer's basket is empty, fetch the next customer
     */
    public void checkoutOnce() {
        // if there's no one in line, return
        if (checkoutLine.getSize() == 0) {
            return;
        }
        else {
            // checkout one item from a customer
            if (currentCustomer.pollItems() == 0) {
                Customer deq = checkoutLine.dequeue();
                getNextCustomer();
                update_dequeue_view(deq);

            }
            currentCustomer.checkoutItem();
        }
    }

    public Node getView() {
        return view;
    }

    public void setView(Node view) {
        this.view = view;
    }

    private void update_dequeue_view(Customer deq) {
        // display the dequeued guy by himself being checked out
        System.out.println("The groups' x translation is " + view.getTranslateX());
        deq.getView().setTranslateY(400);
        // remove it from the group
        deq.getView().getParent().getChildrenUnmodifiable().remove(deq);
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        view.setTranslateY(view.getTranslateY() + Constants.CUSTOMER_RADIUS);
    }


    public String toString() {
        StringBuilder strB = new StringBuilder();
        strB.append("Beginning of checkout line (front of queue)\n");
        for (Customer c: checkoutLine) {
            strB.append(c.toString());
        }
        strB.append("End of queue\n");
        return strB.toString();
    }

    // update method used in visualization
    public void update() {
        checkoutOnce();
    }

}

