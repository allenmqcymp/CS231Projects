/**
 * Allen Ma
 * March 2018
 */


import java.util.NoSuchElementException;
import java.util.Iterator;

/**
 * Queue internally uses a doubly linked list
 * @param <T>
 */
public class MyQueue<T> implements Iterable<T> {

    private class Node {
        Node previous;
        Node next;
        T item;

        public Node(T item) {
            this.item = item;
            previous = null;
            next = null;
        }
    }

    private Node head;
    private Node tail;
    private int n;


    public MyQueue() {
        n = 0;
        head = null;
        tail = null;
    }

    private int size() {
        return n;
    }

    /**
     * Returns the number of elements in the queue
     * @return the number of elements in the queue
     */
    public int getSize() {
        return n;
    }

    private boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Enqueue - add an item to the "end" of the queue
     */
    public void enqueue(T item) {
        // we will always add to the tail of the list
        Node addNode = new Node(item);

        if (isEmpty()) {
            addNode.next = null;
            addNode.previous = null;
            tail = addNode;
            head = tail;
            n++;
        }
        else {
            addNode.next = null;
            addNode.previous = tail;
            tail.next = addNode;
            tail = addNode;
            n++;
        }

    }

    /**
     * Remove and return item of type T from the queue in FIFO fashion
     * @return item of type T in the queue
     */
    public T dequeue() {
        // we will always dequeue from the head of the list
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is already empty");
        }

        if (size() == 1) {
            Node returnNode = head;
            returnNode.previous = null;
            returnNode.next = null;
            n--;
            head = tail = null;
            return returnNode.item;
        }
        else {
            Node returnNode = head;
            head = head.next;
            head.previous.next = null;
            head.previous = null;
            n--;
            return returnNode.item;
        }
    }

    public Iterator<T> iterator() {
        // return an iterator over items in order from front to end
        return new DLLIterator();
    }

    private class DLLIterator implements Iterator<T> {
        private Node current;

        public DLLIterator() {
            current = head;
        }

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException("remove operation not supported");
        }

        public T next() {

            if (!hasNext()) {
                throw new NoSuchElementException("no more elements to iterate over");
            }
            T item = current.item;
            current = current.next;
            return item;
        }
    }

    public T peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is already empty");
        }
        else {
            return head.item;
        }
    }

    private void offer(T item) {
        enqueue(item);
    }

    private T poll() {
        return dequeue();
    }

//    public static void main(String[] args) {
//        // unit tests
//
//        MyQueue<Integer> mq = new MyQueue<>();
//
//        mq.enqueue(1);
//        mq.enqueue(2);
//
//        System.out.println("There should be 2 elements in queue: there are " + mq.size());
//
//        for (int i = 0; i < 1000; i++) {
//            mq.enqueue(i);
//        }
//
//        System.out.println("There should be 1002 elements in queue: there are " + mq.size());
//
//        mq.dequeue();
//
//        System.out.println("There should be 1001 elements in queue: there are " + mq.size());
//
//        MyQueue<String> sq = new MyQueue<>();
//
//        sq.enqueue("hello");
//
//        System.out.println("There should be 1 elements in queue: there are " + sq.size());
//
//        System.out.println("The element is " + sq.peek());
//
//        System.out.println("Removed " + sq.dequeue());
//
//        System.out.println("There should be 0 elements in queue: there are " + sq.size());
//
//        // tests for enqueue and dequeue
//
//        sq.enqueue("second");
//
//        sq.dequeue();
//
//        sq.enqueue("third");
//
//        System.out.println(sq.dequeue());
//
//        MyQueue<Integer> jq = new MyQueue<>();
//
//        for (int i = 1; i < 998; i++) {
//            jq.enqueue(i);
//        }
//
//        System.out.println(jq.dequeue());
//        System.out.println(jq.dequeue());
//        System.out.println(jq.dequeue());
//        System.out.println(jq.dequeue());
//        System.out.println(jq.dequeue());
//
//        // tests for iterator
//        for (Integer i: jq) {
//            System.out.println("Found " + i);
//        }
//    }
}
