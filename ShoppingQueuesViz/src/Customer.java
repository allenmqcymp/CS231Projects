/**
 * Allen Ma
 * March 2018
 */

/**
 * Customer class
 */

import javafx.scene.Node;



public class Customer {

    private int time = 0;

    private strategy strat = strategy.RANDOM;

    private int items;

    private int id;

    private Node view;

    public Customer() {
        items = 1;
    }

    public Customer(int items) {
        this.items = items;
    }

    public Customer(int items, strategy st) {
        this.items = items;
        this.strat = st;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Node getView() {
        return view;
    }

    public void setView(Node view) {
        this.view = view;
    }

    public strategy getStrategy() {
        return strat;
    }

    /**
     * Returns how many items a customer has in his/her cart
     */
    public int pollItems() {
        return items;
    }

    /**
     * Checks out one item
     */
    public void checkoutItem() {
        if (items <= 0) {
            throw new IllegalStateException("Cannot checkout already empty cart");
        }
        else {
            items--;
        }
    }

    public int getTime() {
        return time;
    }

    /**
     * @param t Number of time steps to increment the customer's time
     */
    public void incrementTime(int t) {
        time += t;
    }

    public String toString() {
        return "Customer with " + items + " items and strategy " + strat + " and time " + time +  "\n";
    }
}
