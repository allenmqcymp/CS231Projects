import com.sun.tools.internal.jxc.ap.Const;
import com.sun.tools.javac.code.Attribute;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;

/**
 * Allen Ma
 * March 2018
 */

public class ShoppingQueuesViz extends Application {


    private List<CheckoutLine> lines = new ArrayList<CheckoutLine>();
    private List<Customer> customers = new ArrayList<Customer>();


    // ****** ENTRY FUNCTION ***********

    /**
     * Start of the simulation
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Shopping Line Simulation");

        Pane root = new Pane();

        makeInitialView(root);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                onUpdate();
            }
        };
        timer.start();

        primaryStage.setScene(new Scene(root, Constants.WIDTH + 2 * Constants.PADDING, Constants.HEIGHT + 2 * Constants.PADDING));
        primaryStage.show();
    }

    private void onUpdate() {
        for (CheckoutLine l: lines) {
            l.update();
        }
    }

    // ****** CASHIERS ***********

    private void makeCashiers(int num, Pane root) {
        // draw some rectangles corresponding to the cashier
        for (int i = 0; i < num; i++) {
            Rectangle rec = new Rectangle((Constants.WIDTH - 2 * Constants.PADDING) / num * (i+1), (Constants.HEIGHT * 4/5), Constants.CASHIER_SIZE, Constants.CASHIER_SIZE);
            root.getChildren().add(rec);
        }
    }

    // ****** CHECKOUT LINE ***********

    // show a checkout line on the screen
    private Group processLine(CheckoutLine line, Pane root) {
        Group g = new Group();
        // coordinates always relative to group
        int i = 0;
        for (Customer c: line.getLine()) {
            Circle circ = new Circle(Constants.CUSTOMER_RADIUS / 2, i * (2 * Constants.CUSTOMER_RADIUS), Constants.CUSTOMER_RADIUS);
            g.getChildren().add(circ);
            customers.get(c.getId()).setView(circ);
            i += 1;
        }
        return g;
    }

    /**
     * Random selection should only take one time step
     * @return the index of the random line that the customer should join
     */
    public int selectRandom() {
        Random gen = new Random();
        return gen.nextInt(Constants.numCheckoutLines);
    }


    // ****** MAKE INTIAL VIEW ***********

    private void makeInitialView(Pane root) {

        // init a random object for random customer assignment
        Random gen = new Random();

        // make the cashiers ( static squares at the bottom of the screen)
        makeCashiers(Constants.numCheckoutLines, root);

        // make some lines
        for (int i = 0; i < Constants.numCheckoutLines; i++) {
            lines.add(new CheckoutLine());
        }

        // add some default customers to each line
        for (int i = 0; i < Constants.defaultCustomers; i++) {
            int randomLine = gen.nextInt(Constants.numCheckoutLines);
            Customer defaultCustomer = new Customer(gen.nextInt(10) + 1, strategy.RANDOM);
            defaultCustomer.setId(i);

            // add every customer ever created to customers
            customers.add(defaultCustomer);

            lines.get(randomLine).getLine().enqueue(defaultCustomer);
        }

        // display a line for each cashier station
        for (int i = 0; i < Constants.numCheckoutLines; i++) {
            Group g = processLine(lines.get(i), root);

            // assign the group object as the view of the ith line
            lines.get(i).setView(g);

            // set the group's layout relative to the pane
            g.setLayoutX((Constants.WIDTH - 2 * Constants.PADDING) / Constants.numCheckoutLines * (i + 1));
            g.setLayoutY(Constants.HEIGHT / 5);

            root.getChildren().add(g);
        }

        // assign currentCustomer the head of each queue initially
        for (CheckoutLine l: lines) {
            if (l.getLine().getSize() > 0) {
                l.assignCurrentCustomer(l.getLine().peek());
            }
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}

