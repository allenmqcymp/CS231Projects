/**
 * Allen Ma
 * March 2018
 */

/**
 * Customer class
 */

import java.awt.*;
import java.io.IOException;


public class Customer {

    private static Resources res;

    static {
        try {
            res = new Resources();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int time = 0;

    private int x;
    private int y;

    private strategy strat = strategy.RANDOM;

    private int items;

    public Customer() {
        items = 1;
    }

    public Customer(int items) {
        this.items = items;
    }

    public Customer(int items, strategy st) {
        this.items = items;
        this.strat = st;
    }

    // copy constructor
    public Customer(Customer c) {
        this.items = c.items;
        this.x = c.x;
        this.y = c.y;
        this.strat = c.strat;
        this.time = c.time;
    }

    public strategy getStrategy() {
        return strat;
    }

    /**
     * Returns how many items a customer has in his/her cart
     */
    public int pollItems() {
        return items;
    }

    /**
     * Checks out one item
     */
    public void checkoutItem() {
        if (items <= 0) {
            throw new IllegalStateException("Cannot checkout already empty cart");
        }
        else {
            items--;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Customer copy() {
        return new Customer(this);
    }

    /**
     * Draws based on its internal x and y coordinates
     * @param g
     * @param scale
     */
    public void draw(Graphics g, int scale) {
        Image img = res.getImage("Trolley.png");
        g.drawImage(img, x, y - img.getHeight(null) / 2, null);
        g.drawString(Integer.toString(this.items), x, y);
    }

    public int getTime() {
        return time;
    }

    /**
     * @param t Number of time steps to increment the customer's time
     */
    public void incrementTime(int t) {
        time += t;
    }

    public String toString() {
        return "Customer with " + items + " items and strategy " + strat + " and time " + time +  "\n";
    }
}
