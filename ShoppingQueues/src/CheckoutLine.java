/**
 * Checkoutline is a wrapper class for a  queue (node based DLL queue)
 */

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;


public class CheckoutLine {

    private Customer currentCustomer;
    private MyQueue<Customer> checkoutLine;

    private java.util.List<Customer> customersStats = new ArrayList<Customer>();


    /**
     * Try and fetch resources
     */
    private static Resources res;

    static {
        try {
            res = new Resources();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * x and y are the integral coordinates of the cashier on the screen (absolute coords)
     */

    private int x;
    private int y;

    public CheckoutLine() {
        checkoutLine = new MyQueue<Customer>();
        currentCustomer = null;
    }

    public CheckoutLine(int x, int y, int scale) {
        checkoutLine = new MyQueue<Customer>();
        currentCustomer = null;
        this.x = x;
        this.y = y;
    }


    public MyQueue<Customer> getLine() {
        return checkoutLine;
    }

    /**
     * Get the next customer in the queue
     */
    private void getNextCustomer() {
        if (checkoutLine.getSize() == 0) {
            currentCustomer = null;
            return;
        }
        currentCustomer = checkoutLine.peek();
    }

    /**
     * This method should be called after initializing the customer queue
     * The queue will be null at first, so we have to set currentCustomer manually
     * after we create at least one customer
     */
    public void assignCurrentCustomer(Customer c) {
        if (checkoutLine.getSize() == 0) {
            return;
        }
        currentCustomer = c;
    }

    /**
     * Called from simulation
     * In one timestep, either:
     *  do nothing if the line is empty
     *  or, checkout one item from a customer
     *  when the customer's basket is empty, fetch the next customer
     */
    public void checkoutOnce() {
        // if there's no one in line, return
        if (checkoutLine.getSize() == 0) {
            return;
        }
        else {
            // handle edge case
            if (checkoutLine.getSize() >= 1) {
                assignCurrentCustomer(checkoutLine.peek());
            }
            if (currentCustomer.pollItems() == 0) {
                Customer removed = checkoutLine.dequeue();
                // increment time by 1 of all remaining customers in queue
                for (Customer cs: checkoutLine) {
                    cs.incrementTime(1);
                }
                customersStats.add(removed);
                getNextCustomer();

                if (currentCustomer == null) {
                    return;
                }
            }
            currentCustomer.checkoutItem();
        }
    }

    /**
     * Draws everything  - uses x and y properties of the cashier and of the customer in the line
     * @param g
     * @param scale
     */

    public void draw(Graphics g, int scale) {
        Image cashier = res.getImage("Cashier.png");
        g.drawImage(cashier, x , y - cashier.getHeight(null) / 2, null);

        int i = 0;
        for (Customer c: getLine()) {
            if (c.equals(currentCustomer)) {
                continue;
            }
            c.setX(this.x + Constants.CHECKOUT_OFFSET);
            c.setY(this.y / 5 + (getLine().getSize() - i) * Constants.CUSTOMER_RADIUS);
            c.draw(g, scale);
            i += 1;
        }

        // if currentCustomer has already been assigned
        if (currentCustomer != null) {
            // draw current customer next to the cashier
            currentCustomer.setX(Constants.CHECKOUT_OFFSET + this.x);
            currentCustomer.setY(this.y);
            currentCustomer.draw(g, scale);
        }
    }

    /**
     * updates state for landscape
     */
    public void updateState() {
        if (checkoutLine.getSize() == 0) {
            return;
        }
        else {
            // handle the edge case where customer is added right after reaches 0
            if (checkoutLine.getSize() >= 1) {
                assignCurrentCustomer(checkoutLine.peek());
            }
            if (currentCustomer.pollItems() == 0) {
                Customer removed = checkoutLine.dequeue();
                // add cu
                customersStats.add(removed);

                // update positions
                for (Customer c: getLine()) {
                    c.setY(c.getY() + Constants.CUSTOMER_RADIUS);
                }

                // get the next customer
                getNextCustomer();

                // terminate when we can't get another customer
                if (currentCustomer == null) {
                    return;
                }
            }
            currentCustomer.checkoutItem();
        }
    }

    public List<Customer> getCustomersStats() {
        return customersStats;
    }

    /**
     * useful string repr
     * @return
     */
    public String toString() {
        StringBuilder strB = new StringBuilder();
        strB.append("Beginning of checkout line (front of queue)\n");
        for (Customer c: checkoutLine) {
            strB.append(c.toString());
        }
        strB.append("End of queue\n");
        return strB.toString();
    }

}


