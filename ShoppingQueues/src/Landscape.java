/**
 * Allen Ma
 * March 2018
 */


import java.util.ArrayList;
import java.awt.Graphics;
import java.util.List;
import java.util.Random;

public class Landscape {

    private static int t = 0;

    // how many customers we still need to spawn
    private static int needToSpawn = Constants.NUMCUSTOMERS;

    // display for spawnedCustomers
    private List<Customer> spawnedDisplay = null;

    private int scale;

    private List<CheckoutLine> lines = new ArrayList<CheckoutLine>();
    // master list of customers
    private List<Customer> customers = new ArrayList<Customer>();

    /**
     * Random selection should only take one time step
     * @return the index of the random line that the customer should join
     */
    public int selectRandom() {
        Random gen = new Random();
        return gen.nextInt(Constants.NUMLINES);
    }

    /**
     * Selection by iteration through each checkout line and seeeing how many people it has
     * This should take time proportional to how may checkout lines there are
     * Note that we are not setting the time here, but in the method that determines which one to choose
     */
    public int selectShortest() {
        // set minimum to first line in the lines ie. index 0
        int minCustomers = lines.get(0).getLine().getSize();
        int min = 0;
        for (int j = 0; j < lines.size(); j++) {
            if (lines.get(j).getLine().getSize() < minCustomers) {
                min = j;
            }
        }
        return min;
    }

    /**
     * Selection by randomly choosing two lines, and then determining the smaller of the two lines
     */
    public int selectTwo() {
        Random gen = new Random();
        int random1 = gen.nextInt(Constants.NUMLINES);
        int random2 = gen.nextInt(Constants.NUMLINES);
        while (random1 == random2) {
            random2 = gen.nextInt(Constants.NUMLINES);
        }

        if (lines.get(random1).getLine().getSize() < lines.get(random2).getLine().getSize()) {
            return random1;
        }
        else {
            return random2;
        }
    }

    /**
     * Spawn some customers each round, add them to the top of the list, and add them to different queues
     */
    public List<Customer> spawn(int n) {
        ArrayList<Customer> spawned = new ArrayList<>(n);
        if (Constants.NUMCUSTOMERS % n != 0 || n > Constants.NUMCUSTOMERS) {
            throw new IllegalArgumentException("Spawned number must be a multiple of number of customers; your n is " + n + " and your numCustomers is " + Constants.NUMCUSTOMERS);
        }
        if (needToSpawn == 0) {
            return null;
        }
        else {
            needToSpawn -= n;
            Random gen = new Random();
            for (int i = 0; i < n; i++) {
                spawned.add(new Customer(Constants.MIN_NUM_ITEMS + gen.nextInt(10), strategy.RANDOMN) );
            }
        }
        return spawned;
    }


    public Landscape(int scale) {
        this.scale = scale;
        // make checkout lines
        for (int i = 0; i < Constants.NUMLINES; i++) {
            lines.add(new CheckoutLine((Constants.SCREEN_WIDTH) / Constants.NUMLINES * i, Constants.SCREEN_HEIGHT, scale));
        }

        // assign some default customers to the checkout line
        for (int i = 0; i < Constants.NUMDEFAULT; i++) {
            int randomLine = selectRandom();
            Random gen = new Random();
            Customer c = new Customer(gen.nextInt(5) + 1);
            customers.add(c);
            lines.get(randomLine).getLine().enqueue(c);
        }

        // assign currentCustomer to each line
        for (CheckoutLine line: lines) {
            if (!(line.getLine().getSize() == 0)) {
                line.assignCurrentCustomer(line.getLine().peek());
            }
        }
    }

    public static int getNeedToSpawn() {
        return needToSpawn;
    }

    public List<CheckoutLine> getLines() {
        return lines;
    }

    public int getWidth() {
        return Constants.SCREEN_WIDTH + Constants.PADDING;
    }

    public int getHeight() {
        return Constants.SCREEN_HEIGHT + Constants.PADDING;
    }

    public void draw(Graphics g, int gridScale) {

//         display some text at the start
        g.drawString("Spawned Customers Here:", 0, Constants.SCREEN_HEIGHT / 20);

        // draw spawned display (this is the landscape's duty)
        if (spawnedDisplay != null) {
            for (Customer c: spawnedDisplay) {
                c.draw(g, gridScale);
            }
        }

        for (CheckoutLine line: lines) {
            line.draw(g, gridScale);
        }
    }

    // run one timestep
    public void update() {



        // every 100 time steps, spawn 4 customers
        t += 1;
        if (t % 2 == 0) {
            List<Customer> spawned = spawn(Constants.SPAWN_COUNT);

            // set spawnedDisplay to null once all spawned
            if (needToSpawn == 0) {
                spawnedDisplay = null;
            }

            // deep copy those items and draw them at the top of the screen
            if (spawned != null) {
                spawnedDisplay = new ArrayList<Customer>();
                for (Customer c: spawned) {
                    spawnedDisplay.add(c.copy());
                }
            }

            if (spawnedDisplay != null) {
                // assign elements of spawnedDisplay to the top of the list
                int d = 0;
                for (Customer s: spawnedDisplay) {
                    s.setY(Constants.SCREEN_HEIGHT / 10);
                    s.setX(Constants.SCREEN_HEIGHT / 10 * d);
                    d += 1;
                }
            }


            if (spawned != null) {
                for (Customer c: spawned) {
                    // based on the customer's strategy add him/her to the appropriate queue

                    switch (c.getStrategy()) {
                        case RANDOM:
                            int randomLine = selectRandom();
                            lines.get(randomLine).getLine().enqueue(c);
                            // add one time increment to the player
                            c.incrementTime(1);
                            break;
                        case SHORTEST:
                            int shortestLine = selectShortest();
                            lines.get(shortestLine).getLine().enqueue(c);
                            // add time proportional to the number of lines
                            c.incrementTime(Constants.NUMLINES);
                            break;
                        case RANDOMTWO:
                            int mitzenmacher = selectTwo();
                            lines.get(mitzenmacher).getLine().enqueue(c);
                            // add two time increments
                            c.incrementTime(2);
                            break;
                    }
                }
            }
        }


        for (CheckoutLine l: lines) {
            l.updateState();
        }
    }



}

