

import java.util.ArrayList;
import java.util.List;

/**
 * Class to control simulation
 */
public class CheckoutSimulation {

    private int scale;
    private LandscapeDisplay display;
    private Landscape scape;

    /**
     * Constructor
     * @param scale
     */
    public CheckoutSimulation(int scale) {
        this.scale = scale;
        scape = new Landscape(scale);
        display = new LandscapeDisplay(scape, scale);

    }

    public LandscapeDisplay getDisplay() {
        return display;
    }

    public Landscape getScape() {
        return scape;
    }

    /**
     * Check if finished - checks if it needs to spawn AND whether all of the lines are finished processing
     * @return
     */
    public boolean isFinished() {
        if (Landscape.getNeedToSpawn() != 0) {
            return false;
        }
        for (CheckoutLine l: getScape().getLines()) {
            if (l.getLine().getSize() != 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Runs a simulation
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        CheckoutSimulation cs = new CheckoutSimulation(1);
        cs.getDisplay().repaint();

        /**
         * While game loop
         */
        boolean simulationFinished = false;
        while (!simulationFinished) {
            cs.getScape().update();
            Thread.sleep(500);
            // check for finished conditions:
            simulationFinished = cs.isFinished();

            cs.getDisplay().repaint();
        }

        List<Integer> times = new ArrayList<Integer>();

        // print out statistics
        for (CheckoutLine l: cs.getScape().getLines()) {
            for (Customer c: l.getCustomersStats()) {
                times.add(c.getTime());
            }
        }

        // print out useful stats
        SimpleStats statsObj = new SimpleStats();

        System.out.println("********* Start Stats: Simulation finished *********");
        System.out.println("***** Mean time is: " + statsObj.mean(times) + " units ****");
        System.out.println("***** Standard deviation is: " + statsObj.stdev(times));
        System.out.println("****Parameters are: *****");
        System.out.println("Number of checkout lines: " + Constants.NUMLINES);
        System.out.println("Number of customers: " + Constants.NUMCUSTOMERS);
        System.out.println("Number of default customers: " + Constants.NUMDEFAULT);
        System.out.println("MIN Number of items per customer: " + Constants.MIN_NUM_ITEMS);
        System.out.println("Number of customers spawned at once: " + Constants.SPAWN_COUNT);
        System.out.println("********* End Stats: Simulation finished *********");

    }
}


