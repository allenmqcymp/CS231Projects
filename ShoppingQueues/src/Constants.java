/**
 * Constants class - variables are self-explanatory
 */

public final class Constants {

    public static final int NUMDEFAULT = 30;
    public static final int NUMLINES = 10;
    public static final int NUMCUSTOMERS = 60;
    public static final int SCREEN_WIDTH = 500;
    public static final int SCREEN_HEIGHT = 500;
    public static final int PADDING = 20;

    public static final int CHECKOUT_OFFSET = 30;

    public static final int CUSTOMER_RADIUS = 20;

    public static final int MIN_NUM_ITEMS = 5;

    public static final int SPAWN_COUNT = 4;

    public static final int N_LINES_NUMBER = 3;

}
