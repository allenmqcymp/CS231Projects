/**
 * Strategy class - replace magic numbers with useful words
 */
public enum strategy {
    RANDOM,
    SHORTEST,
    RANDOMTWO,
    RANDOMN;
}
