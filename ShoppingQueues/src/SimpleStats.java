/**
 * Mean and standard deviation functions
 */



import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class SimpleStats {

    public SimpleStats() {

    }

    /**
     * Calculate population standard deviation of list of integers
     *
     * @param l
     * @return double stdev
     */
    public double stdev(List<Integer> l) {
        double m = mean(l);
        double totalSumSq = 0;
        for (Integer i: l) {
            totalSumSq += Math.pow((i - m), 2.0);
        }
        return Math.sqrt(totalSumSq / l.size());
    }

    /**
     * Calculate mean
     * @param l
     * @return double mean
     */
    public double mean(List<Integer> l) {
        int total = 0;
        for (Integer i: l) {
            total += i;
        }
        return (float) total / l.size();
    }

    // tests
    public static void main(String[] args) {
        SimpleStats st = new SimpleStats();
        List<Integer> l = new ArrayList<Integer>(Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1));
        System.out.println(st.mean(l));
        System.out.println(st.stdev(l));



        List<Integer> j = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        System.out.println(st.mean(j));
        System.out.println(st.stdev(j));

        List<Integer> q = new ArrayList<Integer>(Arrays.asList(2, 4));
        System.out.println(st.mean(q));
        System.out.println(st.stdev(q));
    }
}
