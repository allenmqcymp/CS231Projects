import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class Resources {

    private HashMap<String, Image> imageMap = new HashMap<String, Image>();

    public Resources() throws IOException {
        getImages("./resources/");
    }

    /**
     * Try and load all the images from resources
     * @param path
     * @throws IOException
     */
    public void getImages(String path) throws IOException {

        File dir = new File(path);

        File[] dirList = dir.listFiles();

        if (dirList != null) {
            for (File imagePath: dirList) {
                try {
                    Image image = ImageIO.read(imagePath);
                    // add the image to the hashmap
                    imageMap.put(imagePath.getName(), image);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            throw new IOException("Folder " + path + " is null");
        }
    }

    /**
     * Access image by its name
     * @param path
     * @return
     * @throws NoSuchElementException
     */
    public Image getImage(String path) throws NoSuchElementException {
        Image image = imageMap.get(path);
        if (image == null) {
            throw new NoSuchElementException("Couldn't find key in images. Key: " + path);
        }
        else {
            return image;
        }
    }
}


