import java.lang.reflect.Array;
import java.util.*;

/*
 * Allen Ma
 * March 2018
 */

/**
 * A command-line simulation of the shopping queue
 */

public class Simulation {

    private int numCheckoutLines;
    private int numCustomers;


    private int customerCount;

    private int defaultCustomers = 10;

    // lines array list holds all the checkout lines
    // each checkout line itself is  queue
    private ArrayList<CheckoutLine> lines;

    public ArrayList<CheckoutLine> getLines() {
        return lines;
    }

    /**
     *     n checkout lines
     *     m customers
     */
    public Simulation(int n, int m) {
        numCheckoutLines = n;
        numCustomers = m;
        lines = new ArrayList<CheckoutLine>(n);
        customerCount = numCustomers;

        // add n lines to lines arraylist
        for (int i = 0; i < numCheckoutLines; i++) {
            lines.add(new CheckoutLine());
        }
    }


    /**
     * Random selection should only take one time step
     * @return the index of the random line that the customer should join
     */
    public int selectRandom() {
        Random gen = new Random();
        return gen.nextInt(numCheckoutLines);
    }

    /**
     * Selection by iteration through each checkout line and seeeing how many people it has
     * This should take time proportional to how may checkout lines there are
     * Note that we are not setting the time here, but in the method that determines which one to choose
     */
    public int selectShortest() {
        // set minimum to first line in the lines ie. index 0
        int minCustomers = lines.get(0).getLine().getSize();
        int min = 0;
        for (int j = 0; j < lines.size(); j++) {
            if (lines.get(j).getLine().getSize() < minCustomers) {
                min = j;
            }
        }
        return min;
    }

    /**
     * Selection by randomly choosing two lines, and then determining the smaller of the two lines
     */
    public int selectTwo() {
        Random gen = new Random();
        int random1 = gen.nextInt(numCheckoutLines);
        int random2 = gen.nextInt(numCheckoutLines);
        while (random1 == random2) {
            random2 = gen.nextInt(numCheckoutLines);
        }

        if (lines.get(random1).getLine().getSize() < lines.get(random2).getLine().getSize()) {
            return random1;
        }
        else {
            return random2;
        }
    }

    /**
     * Select best from N
     * @param n
     * @return
     */
    public int selectN(int n) {
        Random gen = new Random();
        // java doesn't have list comprehensions <:(
        // even if it did, its syntax would be downright ugly
        List<Integer> javaNoListComp = new ArrayList<Integer>();

        int[] n_lines = new int[n];

        for (int i = 0; i < numCheckoutLines; i++) {
            javaNoListComp.add(i);
        }
        Collections.shuffle(javaNoListComp);

        // select N
        for (int i = 0; i < n; i++) {
            n_lines[i] = javaNoListComp.remove(0);
        }

        // select minimum line from n_lines
        int min_line = n_lines[0];
        for (int j = 0; j < n; j++) {
            if (lines.get(j).getLine().getSize() < lines.get(min_line).getLine().getSize()) {
                min_line = j;
            }
        }
        return min_line;
    }

    public ArrayList<Customer> spawnCustomers(int n) {
        ArrayList<Customer> spawned = new ArrayList<Customer>();
        if (numCustomers % n != 0 || n > numCustomers) {
            throw new IllegalArgumentException("Spawned number must be a multiple of number of customers; your n is " + n + " and your numCustomers is " + numCustomers);
        }

        if (customerCount == 0) {
            return null;
        }
        else {
            customerCount -= n;
//            for (int i = 0; i < n; i++) {
//                spawned.add(new Customer( (Constants.MIN_NUM_ITEMS), strategy.RANDOMN) );
//            }
            // modified version for the mixed bag extension
            for (int i = 0; i < n; i++) {
                if (Math.random() < (double) 1 / 3) {
                    spawned.add(new Customer( (Constants.MIN_NUM_ITEMS), strategy.RANDOM) );
                }
                else if (Math.random() >= (double) 1 / 3 && Math.random() < (double) 2 / 3) {
                    spawned.add(new Customer( (Constants.MIN_NUM_ITEMS), strategy.RANDOMTWO) );
                }
                else {
                    spawned.add(new Customer( (Constants.MIN_NUM_ITEMS), strategy.SHORTEST) );
                }
            }
        }
        return spawned;
    }

    public void runSimulation() {

        Random gen = new Random();

        // steps

        // 1. create some default participants in the store already, and randomly assign them to checkout lines
        for (int i = 0; i < defaultCustomers; i++) {
            int randomLine = gen.nextInt(numCheckoutLines);
            // initialize a random number of items in the basket
            Customer defaultCustomer = new Customer(gen.nextInt(10) + 1);
            lines.get(randomLine).getLine().enqueue(defaultCustomer);
        }

        // assign currentCustomer to each of the lines
        for (CheckoutLine l: lines) {
            if (l.getLine().getSize() > 0) {
                l.assignCurrentCustomer(l.getLine().peek());
            }
        }

        // start of actual simulation

        System.out.println("******Start of Simulation********");

        boolean simulationFinished = false;
        while (!simulationFinished) {

            System.out.println("**********Simulation loop here**************");

            // if the customers list is empty, then terminate the simulation
            if (customerCount == 0) {
                simulationFinished = true;
            }

            // each time step, update checkout one item from each line
            for (CheckoutLine l: lines) {
                l.checkoutOnce();
            }

            // each time step, make 4 customers join the wait
            // 4 is basically the spawning process
            // can be changed
            ArrayList<Customer> spawned = spawnCustomers(Constants.SPAWN_COUNT);

            if (spawned != null) {
                for (Customer c: spawned) {
                    // based on the customer's strategy add him/her to the appropriate queue

                    switch (c.getStrategy()) {
                        case RANDOM:
                            int randomLine = selectRandom();
                            lines.get(randomLine).getLine().enqueue(c);
                            // add one time increment to the player
                            c.incrementTime(1);
                            break;
                        case SHORTEST:
                            int shortestLine = selectShortest();
                            lines.get(shortestLine).getLine().enqueue(c);
                            // add time proportional to the number of lines
                            c.incrementTime(numCheckoutLines);
                            break;
                        case RANDOMTWO:
                            int mitzenmacher = selectTwo();
                            lines.get(mitzenmacher).getLine().enqueue(c);
                            // add two time increments
                            c.incrementTime(2);
                            break;
                        case RANDOMN:
                            int n_selection = selectN(Constants.N_LINES_NUMBER);
                            lines.get(n_selection).getLine().enqueue(c);
                            c.incrementTime(Constants.N_LINES_NUMBER);
                    }
                }
            }

            for (CheckoutLine line: lines) {
                System.out.println(line);
            }

        }
    }

    public static void main(String[] args) {
        // create a simulation instance

        Simulation s = new Simulation(Constants.NUMLINES, Constants.NUMCUSTOMERS);
        s.runSimulation();

        List<Integer> times = new ArrayList<Integer>();

        // print out statistics
        for (CheckoutLine l: s.getLines()) {
            for (Customer c: l.getCustomersStats()) {
                times.add(c.getTime());
            }
        }

//        // Special case - 3 different categories
//        List<Integer> randomTimes = new ArrayList<Integer>();
//        List<Integer> selectTwoTimes = new ArrayList<Integer>();
//        List<Integer> shortestTimes = new ArrayList<Integer>();
//
//        for (CheckoutLine l: s.getLines()) {
//            for (Customer c: l.getCustomersStats()) {
//                if (c.getStrategy() == strategy.RANDOM) {
//                    randomTimes.add(c.getTime());
//                }
//                else if (c.getStrategy() == strategy.SHORTEST) {
//                    shortestTimes.add(c.getTime());
//                }
//                else {
//                    selectTwoTimes.add(c.getTime());
//                }
//            }
//        }

//        SimpleStats statsObj = new SimpleStats();
//
//        // special case print all 3 categories
//        System.out.println("********* Start Stats: Simulation finished *********");
//        System.out.println("***** Mean time  for random is: " + statsObj.mean(randomTimes) + " units ****");
//        System.out.println("***** Mean time  for select two is: " + statsObj.mean(selectTwoTimes) + " units ****");
//        System.out.println("***** Mean time  for shortest is: " + statsObj.mean(shortestTimes) + " units ****");
//
//        System.out.println("***** Standard deviation for random is: " + statsObj.stdev(randomTimes));
//        System.out.println("***** Standard deviation for select two is: " + statsObj.stdev(selectTwoTimes));
//        System.out.println("***** Standard deviation for shortest is: " + statsObj.stdev(shortestTimes));
//
//        System.out.println("Number of customers in random is " + randomTimes.size());
//        System.out.println("Number of customers in select two is " + selectTwoTimes.size());
//        System.out.println("Number of customers in shortest is " + shortestTimes.size());
//
//        System.out.println("****Parameters are: *****");
//        System.out.println("Number of checkout lines: " + Constants.NUMLINES);
//        System.out.println("Number of customers: " + Constants.NUMCUSTOMERS);
//        System.out.println("Number of default customers: " + Constants.NUMDEFAULT);
//        System.out.println("Minimum Number of items per customer: " + Constants.MIN_NUM_ITEMS);
//        System.out.println("Number of customers spawned at once: " + Constants.SPAWN_COUNT);
//        System.out.println("********* End Stats: Simulation finished *********");


        SimpleStats statsObj = new SimpleStats();

        System.out.println("********* Start Stats: Simulation finished *********");
        System.out.println("***** Mean time is: " + statsObj.mean(times) + " units ****");
        System.out.println("***** Standard deviation is: " + statsObj.stdev(times));
        System.out.println("****Parameters are: *****");
        System.out.println("Number of checkout lines: " + Constants.NUMLINES);
        System.out.println("Number of customers: " + Constants.NUMCUSTOMERS);
        System.out.println("Number of default customers: " + Constants.NUMDEFAULT);
        System.out.println("Minimum Number of items per customer: " + Constants.MIN_NUM_ITEMS);
        System.out.println("Number of customers spawned at once: " + Constants.SPAWN_COUNT);
        System.out.println("********* End Stats: Simulation finished *********");
    }

}
