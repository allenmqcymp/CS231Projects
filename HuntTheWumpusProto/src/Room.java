/**
 * Model representation of the room
 * Consists of a graph of vertices
 * @author: Allen Ma
 */

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;



public class Room {

    private Graph graph = new Graph();
    private Vertex[][] vertices = new Vertex[Constants.NUM_HEIGHT][Constants.NUM_WIDE];

    private static Random gen = new Random();

    public Room() {
    }

    public MyPoint setupRoom() {

        // make all the vertices
        for (int i = 0; i < Constants.NUM_HEIGHT; i++) {
            for (int j = 0; j < Constants.NUM_WIDE; j++) {
                vertices[i][j] = new Vertex();
                // set up locations of the vertices
                vertices[i][j].setPoint(new MyPoint(i, j));
            }
        }

        // randomly delete some nodes by setting them to null

        int num_remove = gen.nextInt(Constants.MAX_REMOVED + 1);
        // add all the numbers to a list
        ArrayList<Integer> sample_list = new ArrayList<Integer>();
        for (int i=0; i < Constants.NUM_HEIGHT * Constants.NUM_WIDE; i++) {
            sample_list.add(i);
        }

        // randomly pick out which vertices to remove
        // never remove the starting vertex
        Collections.shuffle(sample_list);
        int removed_count = 0;
        int index = 0;
        while (removed_count < num_remove) {

            int remove_index = sample_list.get(index);
            int row = remove_index / Constants.NUM_WIDE;
            int col = remove_index % Constants.NUM_HEIGHT;

            if (col == Constants.HUNTER_INIT_X && row == Constants.HUNTER_INIT_Y) {
                index++;
            }
            else {
                vertices[row][col] = null;
                index++;
                removed_count++;
            }
        }


        // always set the initial vertex as visited
        vertices[Constants.HUNTER_INIT_X][Constants.HUNTER_INIT_Y].setVisited(true);


        // convert the vertices from array representation to graph representation
        // always ensure that graph is connected
        for (int i = 0; i < Constants.NUM_HEIGHT; i++) {
            for (int j = 0; j < Constants.NUM_WIDE; j++) {

                if (vertices[i][j] != null) {

                    // test north
                    if (hasNeighbor(i, j - 1)) {
                        graph.addEdge(vertices[i][j], Direction.North, vertices[i][j - 1]);
                    }

                    // test south
                    if (hasNeighbor(i, j + 1)) {
                        graph.addEdge(vertices[i][j], Direction.South, vertices[i][j + 1]);
                    }

                    // test east
                    if (hasNeighbor(i + 1, j)) {
                        graph.addEdge(vertices[i][j], Direction.East, vertices[i + 1][j]);
                    }

                    // test west
                    if (hasNeighbor(i - 1, j)) {
                        graph.addEdge(vertices[i][j], Direction.West, vertices[i - 1][j]);
                    }
                }

            }
        }

        assert (graph.isConnected());



        // since the graph is connected, anywhere is reachable
        // calculate distance of every point from starting point
        // add those nodes of greater than a certain distance to an arraylist
        ArrayList<Vertex> validVertices = new ArrayList<>();
        graph.shortestPathAllInferior(vertices[Constants.HUNTER_INIT_X][Constants.HUNTER_INIT_Y]);
        for (Vertex v: graph.getNodes()) {
            if (v.getCost() >= Constants.MIN_DIST) {
                validVertices.add(v);
            }
        }
        // get random vertex to put wumpus in
        Vertex wumpus_vertex = validVertices.get( gen.nextInt(validVertices.size()) );
        MyPoint wumpus_loc = wumpus_vertex.getPoint();

        // set the tiles of those distance 2 away from the wumpus as bloody tiles
        graph.clearMarked();
        graph.clearCost();
        graph.shortestPathAllInferior(wumpus_vertex);
        for (Vertex v: graph.getNodes()) {
            // minimum distance 2
            if (v.getCost() <= 2) {
                v.setNearby(true);
            }
        }

        return wumpus_loc;

    }

    public Vertex[][] getVertices() {
        return vertices;
    }

    public Graph getGraph() {
        return graph;
    }

    /**
     * Check if move is allowed
     * @param d direction
     * @param x - current x
     * @param y - current y
     * @return
     */
    public boolean canMakeMove(Direction d, int x, int y) {
        // based on the graph, check if you can make a valid move
        return (vertices[x][y].getNeighbor(d) != null);
    }

    /**
     * Check if cell has legal neighbor - ie. on grid
     * @param x
     * @param y
     * @return
     */
    private boolean hasNeighbor(int x, int y) {
        return (y < vertices.length && y >= 0) && (x < vertices[0].length && x >= 0) && (vertices[x][y] != null);
    }

    /**
     * Draws room according to current state
     * @param gc
     */
    public void drawRoom(GraphicsContext gc) {

        Image bridgeHoriz = new Image("/bridge_horiz.png");
        Image bridgeVert = new Image("/bridge_vert.png");

        for (int row = 0; row < vertices.length; row++) {
            for (int col = 0; col < vertices[row].length; col++) {

                Vertex v = vertices[row][col];

                // vertices[0][3] means coordinate (0, 3), which is (0, 0) -> (1, 0) -> (2, 0) -> (3, 0)
                // which, if translated to row, col, means COLUMN 0, ROW 3, NOT ROW 0 COLUMN 3
                // my coordinate system is transposed
                if (v == null) {
                    gc.drawImage(Vertex.getNullImage(), row * Constants.VERTEX_SIZE, col * Constants.VERTEX_SIZE);
                }
                else {
                    gc.drawImage(v.getImage(), row * Constants.VERTEX_SIZE, col * Constants.VERTEX_SIZE);

                    // don't draw bridges if not visited
                    if (v.isVisited()) {
                        // for each of the image's neighbors draw the appropriate bridges
                        if (v.getNeighbor(Direction.East) != null) {
                            gc.drawImage(bridgeHoriz, row * Constants.VERTEX_SIZE + 30, col * Constants.VERTEX_SIZE + 20);
                        }
                        if (v.getNeighbor(Direction.West) != null) {
                            gc.drawImage(bridgeHoriz, row * Constants.VERTEX_SIZE, col * Constants.VERTEX_SIZE + 20);
                        }
                        if (v.getNeighbor(Direction.South) != null) {
                            gc.drawImage(bridgeVert, row * Constants.VERTEX_SIZE + 20, col * Constants.VERTEX_SIZE + 30);
                        }
                        if (v.getNeighbor(Direction.North) != null) {
                            gc.drawImage(bridgeVert, row * Constants.VERTEX_SIZE + 20, col * Constants.VERTEX_SIZE);
                        }
                    }
                }

            }
        }

    }
}
