import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 * Since this is a priority queue, it will be a max heap, with the element
 * with the highest priority as the root element
 * @param <T>
 */

public class PQHeap<T> {

    /**
     * Array based implementation of heap
     * We use object to store a value T
     * since java does not support generic typed arrays
     */
    private Object[] heap;
    private int size;
    private Comparator<T> comp;

    private void resize(int capacity) {
        Object[] newHeap = new Object[capacity];
        for (int i = 0; i < size; i++) {
            newHeap[i] = heap[i];
        }
        heap = newHeap;
    }

    public PQHeap(Comparator<T> comparator) {
        heap = new Object[2];
        size = 0;
        comp = comparator;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * The procedure is to always add the element to the end of the heap, representing the
     * rightmost element of the nearly-complete binary tree
     * then, we reheap up as necessary to generate a valid heap maintaining all the properties
     * note, that we might have to resize - this is simply copying the old elements in the array
     * to a new array of a new size, with the elements in the exact same order
     * @param obj
     */
    public void add(T obj) {
        if (size == 0) {
            heap[0] = obj;
            size++;
            return;
        }
        if (size == heap.length) resize(2 * size);

        // parent of a right child is (i + 1) // 2
        // parent of a left child is i // 2
        // this is floor division in Python - in Java it's just /
        // add to end of the heap then reheap up
        heap[size++] = obj;
        reheapUp(size - 1);
    }

    /**
     * Reheap up takes index of node and
     * determines whether it is a left or right node
     * it then checks its parent node
     * if its parent node is less, then it will perform a swap
     */
    private void reheapUp(int i) {
        if (i == 0) {
            return;
        }
        else {
            int parent_index;
            // left child is always odd
            if (i % 2 == 1) {
                // find parent
                parent_index = i / 2;
            }
            // right child is always even
            else {
                parent_index = (i - 1) / 2;
            }

            if (comp.compare((T) heap[parent_index], (T) heap[i]) >= 0) {
                return;
            }
            else {
                swapElements(i, parent_index);
                reheapUp(parent_index);
            }
        }
    }

    /**
     * The method is to swap the root element (index 0),
     * with the rightmost element
     * then, re-heap down the heap starting from the root element
     * return the original root element that has now been moved to the end of the array
     * and delete that node by setting it to null
     * @return
     */
    public T remove() {
        if (size <= 0) {
            throw new NoSuchElementException("Cannot remove from an empty heap");
        }
        if (size == 1) {
            T ret_val = (T) heap[0];
            heap[0] = null;
            size--;
            return ret_val;
        }
        // first, swap the root element with the last element
        swapElements(0, size - 1);
        // save the return value
        T ret_val = (T) heap[size - 1];
        // delete the final node and decrement size (decrement size, the use the new size to access)
        heap[--size] = null;

        if (size == 1) return ret_val;

        reheapDown(0);
        return ret_val;
    }

    /**
     * Reheaps the heap data struture starting from index i
     */
    private void reheapDown(int i) {
        // compares i with both its children
        // swaps i with the SMALLER of its two children, since
        // we want the larger element to bubble up, since this is a max heap

        // the left child is given by 2i + 1
        // the right child is given by 2i + 2

        int lchild_index = 2 * i + 1;
        int rchild_index = 2 * i + 2;

        // both left child and right child don't exist
        if (lchild_index > size - 1 && rchild_index > size - 1) {
            return;
        }

        // left child present, but right child doesn't exist
        if (lchild_index <= size - 1 && rchild_index > size - 1) {
            // our current element has higher precedence than the left child
            // so we don't need to do anything
            if (comp.compare((T) heap[i], (T) heap[lchild_index]) >= 0) return;
            else {
                // we need to swap
                swapElements(i, lchild_index);
                // reheap recursively from the left child
                reheapDown(lchild_index);
            }
        }
        // both left and right child exist
        else {
            int larger_index;
            // first see which of the left or right child is larger
            if (comp.compare( (T) heap[lchild_index], (T) heap[rchild_index]) > 0 ) larger_index = lchild_index;
            else larger_index = rchild_index;

            if (comp.compare((T) heap[i], (T) heap[larger_index]) >= 0) return;
            else {
                swapElements(i, larger_index);
                reheapDown(larger_index);
            }

        }

    }

    /**
     * swaps two element in the array
     * @param i - index i at which to swap
     * @param j - index j at which to swap
     */
    private void swapElements(int i, int j) {
        Object temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }

    /**
     * Prints the state of the heap when it is invoked
     * @return
     */
    public String toString() {
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < size; i++) {
            strB.append("index: " + i + " Value: " + heap[i] + "\n");
        }
        return strB.toString();
    }


}


