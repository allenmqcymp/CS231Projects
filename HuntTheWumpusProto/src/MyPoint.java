/**
 * Simple point class to encapsulate x and y coordinates
 * Created to represent a "tuple" in Python
 * @author: Allen Ma
 */
public class MyPoint {

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;

    }
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString() {
        return x + ", " + y;
    }
}
