import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class StartScreen extends Application {

    private Scene startScene;

    @Override
    public void start(Stage primaryStage) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/StartScreen.fxml") );
        Parent root = loader.load();
        StartScreenController controller = (StartScreenController) loader.getController();
        controller.setPrimaryStage(primaryStage);

        primaryStage.setTitle("Hunt the Wumpus");

        startScene = new Scene(root, 300, 300);

        primaryStage.setScene(startScene);
        primaryStage.show();
    }

    public Scene getStartScene() {
        return startScene;
    }

}
