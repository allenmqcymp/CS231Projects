/**
 * Controller for the StartScreen.FXML file
 * @author: Allen Ma
 */


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.stage.Stage;


public class StartScreenController {

    private Stage primaryStage;

    @FXML
    public Label instructions_lbl;

    @FXML
    public Label title_lbl;

    public StartScreenController() {

    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void handleBeginAction(ActionEvent e) {

        // create a hunt the wumpus instance
        HuntTheWumpus huntthewumpus = new HuntTheWumpus(primaryStage);

        primaryStage.setScene(huntthewumpus.getGameScene());

    }

    @FXML
    private void initialize() {
        instructions_lbl.setText("Arrow keys to move. AWSD to rotate. Spacebar to fire arrow. If you " +
                "misfire or collide with the wumpus you lose. Good luck.");
        instructions_lbl.setAlignment(Pos.CENTER);
        title_lbl.setAlignment(Pos.CENTER);
    }
}
