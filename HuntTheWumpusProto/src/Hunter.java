import javafx.scene.image.Image;

/**
 * @author: Allen Ma
 * Hunter class
 */
public class Hunter {

    // images
    private Image image_up = new Image("/link_up.png");
    private Image image_down = new Image("/link_down.png");
    private Image image_left = new Image("/link_left.png");
    private Image image_right = new Image("/link_right.png");

    private Image image = image_left;
    // initial starting pos
    private int pos_x = Constants.HUNTER_INIT_X;
    private int pos_y = Constants.HUNTER_INIT_Y;

    // moving or shooting
    private State state = State.Move;
    private Direction orientation = Direction.West;

    public static enum State {Move, Shoot};

    public Hunter() {
    }

    /**
     * Getters and setters
     */
    public int getPos_x() {
        return pos_x;
    }

    public int getPos_y() {
        return pos_y;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    public Image getImage() {
        return image;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setOrientation(Direction orientation) {
        this.orientation = orientation;
    }

    public Direction getOrientation() {
        return orientation;
    }

    /**
     * Getters and setters end
     */

    /**
     * Depending on the state and orientation, set the appropriate image
     * @param d
     */
    public void setImage(Direction d) {
        if (state == State.Move) {
            switch (d) {
                case South:
                    image = image_down;
                    break;
                case North:
                    image = image_up;
                    break;
                case West:
                    image = image_left;
                    break;
                case East:
                    image = image_right;
                    break;
            }
        }
        else if (state == State.Shoot) {
            switch (d) {
                case South:
                    image = new Image("/link_shoot_down.png");
                    break;
                case North:
                    image = new Image("/link_shoot_up.png");
                    break;
                case West:
                    image = new Image("/link_shoot_left.png");
                    break;
                case East:
                    image = new Image("/link_shoot_right.png");
                    break;
            }
        }

    }

    /**
     * Adjust coordinates of the player
     * @param d
     */
    public void move(Direction d) {
        switch (d) {
            case South:
                pos_y += 1;
                break;
            case North:
                pos_y -= 1;
                break;
            case East:
                pos_x += 1;
                break;
            case West:
                pos_x -= 1;
                break;
        }
    }
}
