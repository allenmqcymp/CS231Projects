/**
 * @author: Allen Ma
 * Main class for the game
 */

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class HuntTheWumpus {

    /**
     * Rendering is all done on the canvas
     */
    private final Canvas canvas = new Canvas( Constants.WIDTH, Constants.HEIGHT );
    private final Group root = new Group();

    // make a new scene object
    private final Scene gameScene = new Scene(root);

    private boolean gameOver = false;
    private boolean won = false;
    private AnimationTimer timer;

    // make a room - representing all the nodes
    private Room room = new Room();
    private GraphicsContext gc = canvas.getGraphicsContext2D();
    private Hunter hunter = new Hunter();
    private Wumpus wumpus = new Wumpus();

    private Alert alertMsg = new Alert(Alert.AlertType.INFORMATION);

    // getter to get the scene
    public Scene getGameScene() {
        return gameScene;
    }

    // call start method on startup
    public HuntTheWumpus(Stage theStage) {
        start(theStage);
    }

    /**
     * Handles - gameover
     * @param theStage - stage
     */
    private void handleGameover(Stage theStage) {
        if (won) {
            alertMsg.setContentText("You killed the Wumpus!!!\n Press OK to restart");
        }
        else {
            alertMsg.setContentText("You lost!\n Press OK to restart");
        }
        alertMsg.setOnHidden(e -> {
            theStage.setScene(new HuntTheWumpus(theStage).getGameScene());
        });
        alertMsg.show();
    }

    /**
     * Runs the logic of the game
     * @param theStage
     */
    private void runLogic(Stage theStage) {
        // check to see if gameover
        if (gameOver) {
            timer.stop();
            handleGameover(theStage);
        }

        // check to see if the hunter has reached the square of the wumpus
        int pos_x = hunter.getPos_x();
        int pos_y = hunter.getPos_y();
        if (pos_x == wumpus.getPos_x() && pos_y == wumpus.getPos_y()) {
            wumpus.setMet(true);
            gameOver = true;
            won = false;

        }
        if (hunter.getState() == Hunter.State.Shoot) {
            // check the orientation
            Direction arrow_dir = hunter.getOrientation();
            Vertex shot_vertex = room.getVertices()[pos_x][pos_y].getNeighbor(arrow_dir);
            if (shot_vertex != null &&
                    (shot_vertex.getPoint().getX() == wumpus.getPos_x() && shot_vertex.getPoint().getY() == wumpus.getPos_y()) ) {
                // reveal the wumpus
                shot_vertex.setVisited(true);
                wumpus.setMet(true);
                gameOver = true;
                won = true;
            }
            else {
                gameOver = true;
                won = false;
            }
        }
    }

    /**
     * Startup function
     * @param theStage
     */
    public void start(Stage theStage)
    {
        theStage.setTitle( "Hunt the Wumpus" );

        root.getChildren().add( canvas );

        // setup the room, getting the wumpus location
        MyPoint wumpus_loc = room.setupRoom();

        System.out.println("INFO: Finished setting up the room");

        wumpus.setPos_x(wumpus_loc.getX());
        wumpus.setPos_y(wumpus_loc.getY());

        // setup graph
        Vertex[][] vertices = room.getVertices();

        room.drawRoom(gc);

        gc.drawImage( hunter.getImage(), hunter.getPos_x() * Constants.VERTEX_SIZE, hunter.getPos_y() * Constants.VERTEX_SIZE);


        /**
         * Event handling done here - a key press event handler is registered here!
         */
        // implicit handle event called here
        gameScene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            KeyCode k = key.getCode();
            // get the current position of the hunter
            int pos_x = hunter.getPos_x();
            int pos_y = hunter.getPos_y();
            if (k == KeyCode.RIGHT) {
                if (room.canMakeMove(Direction.East, pos_x, pos_y)) {
                    hunter.move(Direction.East);
                    hunter.setImage(Direction.East);
                    hunter.setOrientation(Direction.East);
                    vertices[pos_x][pos_y].getNeighbor(Direction.East).setVisited(true);
                }
            }
            else if (k == KeyCode.LEFT) {
                if (room.canMakeMove(Direction.West, pos_x, pos_y)) {
                    hunter.move(Direction.West);
                    hunter.setImage(Direction.West);
                    hunter.setOrientation(Direction.West);
                    vertices[pos_x][pos_y].getNeighbor(Direction.West).setVisited(true);
                }

            }
            else if (k == KeyCode.DOWN) {
                if (room.canMakeMove(Direction.South, pos_x, pos_y)) {
                    hunter.move(Direction.South);
                    hunter.setImage(Direction.South);
                    hunter.setOrientation(Direction.South);
                    vertices[pos_x][pos_y].getNeighbor(Direction.South).setVisited(true);
                }

            }
            else if (k == KeyCode.UP) {
                if (room.canMakeMove(Direction.North, pos_x, pos_y)) {
                    hunter.move(Direction.North);
                    hunter.setImage(Direction.North);
                    hunter.setOrientation(Direction.North);
                    vertices[pos_x][pos_y].getNeighbor(Direction.North).setVisited(true);
                }

            }
            else if (k == KeyCode.A) {
                // rotate hunter to the west-orientation
                hunter.setImage(Direction.West);
                hunter.setOrientation(Direction.West);
            }
            else if (k == KeyCode.W) {
                // rotate hunter to the north-orientation
                hunter.setImage(Direction.North);
                hunter.setOrientation(Direction.North);
            }
            else if (k == KeyCode.S) {
                // rotate hunter to the south-orientation
                hunter.setImage(Direction.South);
                hunter.setOrientation(Direction.South);
            }
            else if (k == KeyCode.D) {
                // rotate hunter to the east-orientation
                hunter.setImage(Direction.East);
                hunter.setOrientation(Direction.East);
            }
            // arrow shoot
            else if (k == KeyCode.SPACE) {
                // set the hunter arrow image corresponding to a certain direction
                hunter.setState(Hunter.State.Shoot);
                hunter.setImage(hunter.getOrientation());
            }

        });

        /**
         * Game loop - in the form of an animation timer
         */
        final long startNanoTime = System.nanoTime();

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                runLogic(theStage);
                room.drawRoom(gc);
                gc.drawImage(hunter.getImage(), hunter.getPos_x() * Constants.VERTEX_SIZE, hunter.getPos_y() * Constants.VERTEX_SIZE);
                if (wumpus.isMet()) {
                    gc.drawImage(wumpus.getImage(), wumpus.getPos_x() * Constants.VERTEX_SIZE, wumpus.getPos_y() * Constants.VERTEX_SIZE);
                }
            }
        };

        timer.start();

        // display the window
        theStage.show();

    }

}
