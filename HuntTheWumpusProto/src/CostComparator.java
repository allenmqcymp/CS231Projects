import java.util.Comparator;

/**
 * Compares costs of two nodes - because we want to use a min heap and the default is max heap
 * We just use a hacky swap of comparator operations!
 * @author: Allen Ma
 */

public class CostComparator implements Comparator<Vertex> {
    public int compare( Vertex v1, Vertex v2 ) {
        return v2.getCost() - v1.getCost();
    }
}
