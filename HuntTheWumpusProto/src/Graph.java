import java.util.ArrayList;
import java.util.PriorityQueue;

public class Graph {

    // arraylist to hold all the nodes
    private ArrayList<Vertex> nodes;

    public Graph() {
        nodes = new ArrayList<>();
    }

    /**
     * establishes a bidirectional link between v1 and v2
     * @param v1
     * @param dir
     * @param v2
     */
    void addEdge(Vertex v1, Direction dir, Vertex v2) {
        // connect both ways
        v1.connect(v2, dir);
        v2.connect(v1, Vertex.opposite(dir));
        if (!nodes.contains(v1)) {
            nodes.add(v1);
        }
        if (!nodes.contains(v2)) {
            nodes.add(v2);
        }
    }

    /**
     * Uses Dijkstra's algorithm to find the shortest distance between all nodes in the graph
     * @param v0
     *
     * Psuedocode is thus:
         * Given: a graph G and starting vertex v0 in G

        Initialize all vertices in G to be unmarked and have infinite cost

        Create a priority queue, q, that orders vertices by lowest cost

        Set the cost of v0 to 0 and add it to q

        while q is not empty:
            let v be the vertex in q with lowest cost
            remove v from q
            mark v as visited
            for each vertex w that neighbors v:
                if w is not marked and v.cost + 1 < w.cost:
                    w.cost = v.cost + 1
                    add w to q

        Output: the cost field of each vertex v in G is the shortest distance from v0 to v.
     */
    void shortestPathAllInferior(Vertex v0) {
        for (Vertex v : nodes) {
            v.setMarked(false);
            v.setCost(Integer.MAX_VALUE);
        }
        PQHeap<Vertex> pque = new PQHeap<>(new CostComparator());
        v0.setCost(0);
        pque.add(v0);
        while (!pque.isEmpty()) {
            Vertex v = pque.remove();
            v.setMarked(true);

            for (Vertex w : v.getNeighbors()) {
                if ( (!w.isMarked()) && (v.getCost() + 1 < w.getCost()) ) {
                    w.setCost(v.getCost() + 1);
                    pque.add(w);
                }
            }
        }
    }

    /**
     * Checks if a graph is connected or not using a dfs
     * This is essential as a prerequisite for using Dijkstra's algorithm
     */
    public boolean isConnected() {
        clearMarked();
        dfs(nodes.get(0));
        for (Vertex v: nodes) {
            if (!v.isMarked()) return false;
        }
        return true;
    }

    private void dfs(Vertex v) {
        if (!v.isMarked()) v.setMarked(true);
        else {
            return;
        }
        for (Vertex nei : v.getNeighbors()) {
            dfs(nei);
        }
    }

    public ArrayList<Vertex> getNodes() {
        return nodes;
    }

    /**
     * Uses Floyd's algorithm to find the shortest path between all nodes in the graph
     * @param v0
     */
    void shortestPathAll(Vertex v0) {

    }

    /**
     * Uses dijsktra's algorithm to find the distance of the shortest path between two specified nodes
     * @return the distance of the shortest path
     * The assumption is that between two nodes, the cost is 1
     * Furthermore, the nodes are undirected
     */
    public int shortestPath(Vertex v0, Vertex v1) {
        return 1;
    }

    /**
     * Clear nodes by ensuring that they are all "unmarked"
     */
    public void clearMarked() {
        for (Vertex v: nodes) {
            v.setMarked(false);
        }
    }

    /**
     * Clear nodes - the cost of all nodes is set to 1
     */
    public void clearCost() {
        for (Vertex v: nodes) {
            v.setCost(1);
        }
    }

    public int vertexCount() {
        return nodes.size();
    }

    public static void main(String[] args) {
        Vertex v0 = new Vertex("v0");
        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");
        Vertex v5 = new Vertex("v5");
        Vertex v6 = new Vertex("v6");
        Vertex v7 = new Vertex("v7");
        Graph g = new Graph();

        g.addEdge(v0, Direction.East, v1);
        g.addEdge(v1, Direction.South, v2);
        g.addEdge(v2, Direction.East, v5);
        g.addEdge(v5, Direction.North, v4);
        g.addEdge(v5, Direction.East, v7);
        g.addEdge(v4, Direction.North, v3);
        g.addEdge(v4, Direction.East, v6);
        g.addEdge(v7, Direction.South, v7);

        System.out.println("Is it connected? " + g.isConnected());

        System.out.println(g.vertexCount());

        for (Vertex v: g.getNodes()) {
            System.out.println(v);
        }

    }
}
