import java.util.HashMap;
import java.util.*;

import javafx.scene.image.Image;

public class Vertex implements Comparable<Vertex> {

    private int cost;
    private boolean marked;
    private HashMap<Direction, Vertex> edgeMap;
    private static Image image = new Image("/tile.png");
    private static Image unvisitedImage = new Image("/blacksquare.png");
    private static Image nullImage = new Image("/blacksquare.png");
    private static Image nearbyImage = new Image("/tile_blood.png");
    private String id;
    private MyPoint point;
    private boolean visited = false;

    private boolean nearby = false;

    public Vertex() {
        edgeMap = new HashMap<>();
        cost = 1;
        marked = false;
    }

    public Vertex(int cost) {
        this.cost = cost;
        edgeMap = new HashMap<>();
        marked = false;
    }

    public Vertex(String id) {
        this.id = id;
        cost = 1;
        edgeMap = new HashMap<>();
        marked = false;
    }

    public void connect(Vertex other, Direction dir) {
        // unidirectional connection
        // add the entry to the hashmap
        edgeMap.put(dir, other);
    }

    public boolean isConnecte(Vertex other, Direction dir) {
        return (edgeMap.get(dir) == other) && (other.getEdgeMap().get(Vertex.opposite(dir)) == this);
    }

    public Vertex getNeighbor(Direction dir) {
        // returns null if no neightbors
        return edgeMap.get(dir);
    }

    public Collection<Vertex> getNeighbors() {
        return edgeMap.values();
    }

    public String toString() {
        String str = "Cost : " + cost + "\n";
        str += "Id : " + id + " \n";
        str += "Marked : " + marked + "\n";
        str += "Number of neighbors " + getNeighbors().size();

        return str;
    }

    public int compareTo(Vertex v) {
        if (this.cost == v.cost) return 0;
        else if (this.cost > v.cost) return 1;
        else return -1;
    }

    /**
     * Getters and setters start
     */
    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image getImage() {
        if (visited) {
            if (nearby) return nearbyImage;
            return image;
        }
        else {
            return unvisitedImage;
        }
    }

    public static Image getUnvisitedImage() {
        return unvisitedImage;
    }

    public static Image getNearbyImage() {
        return nearbyImage;
    }

    public static Image getNullImage() {
        return nullImage;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public HashMap<Direction, Vertex> getEdgeMap() {
        return edgeMap;
    }

    public MyPoint getPoint() {
        return point;
    }

    public void setPoint(MyPoint point) {
        this.point = point;
    }

    public boolean isNearby() {
        return nearby;
    }

    public void setNearby(boolean nearby) {
        this.nearby = nearby;
    }

    /**
     * Getters and setters end
     */

    public static Direction opposite(Direction d) {
        Direction rd = null;
        switch (d) {
            case East:
                rd = Direction.West;
                break;
            case North:
                rd = Direction.South;
                break;
            case South:
                rd = Direction.North;
                break;
            case West:
                rd = Direction.East;
                break;
            default:
                System.out.println("Unexpected dir");
                break;
        }
        return rd;
    }

    // unit tests
    public static void main(String[] args) {
        Vertex v1 = new Vertex();
        Vertex v2 = new Vertex();
        Vertex v3 = new Vertex();
        Vertex v4 = new Vertex();
        v1.connect(v3, Direction.East);
        v2.connect(v3, Direction.East);
        v4.connect(v1, Direction.North);
        v1.connect(v4, opposite(Direction.North));
        System.out.println("v1");
        System.out.println(v1);
        System.out.println("v2");
        System.out.println(v2);
        System.out.println("v3");
        System.out.println(v3);
        System.out.println("v4");
        System.out.println(v4);
    }
}


