/**
 * Entry point of code - invokes launch of the StartScreen class, creating a startscreen
 * @author: Allen Ma
 */

import javafx.application.Application;
public class Main {

    public static void main(String[] args) {
        Application.launch(StartScreen.class, args);
    }
}
