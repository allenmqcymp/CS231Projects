/**
 * @author: Allen Ma
 * Enumeration of directions
 */
public enum Direction {North, East, South, West};
