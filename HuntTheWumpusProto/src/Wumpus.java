/**
 * Model class for the Wumpus
 * @author: Allen Ma
 */

import javafx.scene.image.Image;

public class Wumpus {

    private Image image = new Image("/wumpus.png");
    private int pos_x;
    private int pos_y;

    private boolean met = false;

    public Wumpus() {

    }

    public void setMet(boolean met) {
        this.met = met;
    }

    public boolean isMet() {
        return met;
    }

    public Image getImage() {
        return image;
    }

    public int getPos_y() {
        return pos_y;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public int getPos_x() {
        return pos_x;
    }
}
