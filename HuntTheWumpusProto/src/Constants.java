/**
 * @author: Allen Ma
 * Constants Class
 */

public class Constants {

    public static final int NUM_WIDE = 10;
    public static final int NUM_HEIGHT = 10;

    // maximum number removed is 20%
    public static final int MAX_REMOVED = NUM_WIDE * NUM_HEIGHT / 5;

    public static final int VERTEX_SIZE = 50;

    public static final int WIDTH = NUM_WIDE * VERTEX_SIZE;
    public static final int HEIGHT = NUM_HEIGHT * VERTEX_SIZE;
    public static final int HUNTER_INIT_X = NUM_HEIGHT - 1;
    public static final int HUNTER_INIT_Y = NUM_WIDE - 1;

    public static final int MIN_DIST = NUM_HEIGHT;

}
