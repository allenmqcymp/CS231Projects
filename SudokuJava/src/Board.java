/**
 * Allen Ma
 * Feb 2018
 * Board Class (practice for IO)
 */

import java.awt.*;
import java.util.Scanner;
import java.io.*;



import java.io.*;

public class Board {

    private Cell[][] cells;
    public static int bsize = 9;

    public Board() {
        cells = new Cell[Board.bsize][Board.bsize];
        for (int i = 0; i < Board.bsize; i++) {
            for (int j = 0; j < Board.bsize; j++) {
                cells[i][j] = new Cell(i, j);
            }
        }
    }

    public Cell[][] getCells() {
        return cells;
    }

    /**
     *
     * @return String with the textual (cmd line) representation of the board
     */
    public String toString() {
        StringBuilder strB = new StringBuilder();
        for (int i = 0; i < Board.bsize; i++) {
            if (i % 3 == 0) {
                strB.append("\n");
            }
            strB.append("\n");
            for (int j = 0; j < Board.bsize; j++) {
                if (j % 3 == 0) {
                    strB.append("   ");
                }
                strB.append(cells[i][j].getValue());
                strB.append(" ");
            }
        }
        return strB.toString();
    }

    public boolean read(String filename) {
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader bfr = new BufferedReader(fr);

            while (true) {
                String res = bfr.readLine();
                if (res == null) break;
                String[] strArr = res.split("[ ]+");
                System.out.println(res);
            }
            bfr.close();
            fr.close();
            return true;
        }
        catch(FileNotFoundException ex) {
            System.out.println("Board.read():: unable to open file " + filename );
        }
        catch(IOException ex) {
            System.out.println("Board.read():: error reading file " + filename);
        }

        return false;
    }

    /**
     * Reads the first 81 integer values (9x9) grid and assigns them by read order to Cell at row column
     * In row major order - eg. row 1, col 1, 2, 3 ... -> row 2, col, 1, 2, 3 ... etc.
     * If you give data in the wrong format, then it's your own fault!
     * @param filename
     */
    public void betterRead(String filename) {
        Scanner sc = null;
        int row_count = 0;
        int col_count = 0;
        try {
            sc = new Scanner(new FileReader(filename));

            while (sc.hasNext()) {
                // make the cell take that value
                int readValue = sc.nextInt();
                cells[row_count][col_count].setValue(readValue);
                // lock that value if nonempty (in this case, nonempty means nonzero)
                if (readValue > 0 && readValue <= 9) {
                    System.out.println("Read " + readValue + " locking row " + row_count + " and col " + col_count);
                    cells[row_count][col_count].setLocked(true);
                }
                else if (readValue < 0 || readValue > 9) {
                    throw new IllegalArgumentException("Character" + readValue + "not between 0 - 9");
                }
                else {
                    // 0
                    // by default false but whatever
                    cells[row_count][col_count].setLocked(false);
                }

                if (row_count == Board.bsize - 1 && col_count == Board.bsize - 1) {
                    break;
                }
                else {
                    // prepare for the next value
                    col_count++;
                    if (col_count % Board.bsize == 0) {
                        col_count = 0;
                        row_count++;
                    }
                }

            }
        }
        catch (FileNotFoundException e2) {
            System.out.println("Unable to find filename" + filename);
            System.out.println("Error: " + e2);
        }
        finally {
            if (sc != null) {
                sc.close();
            }
        }
    }

    public int getCols() {
        return Board.bsize;
    }

    public int getRows() {
        return Board.bsize;
    }

    public Cell get(int r, int c) {
        if ((r >= Board.bsize || r < 0) ||
                (c >= Board.bsize || c < 0)) {
            throw new IndexOutOfBoundsException();
        }
        return cells[r][c];

    }

    public boolean isLocked(int r, int c) {
        return get(r, c).isLocked();
    }

    public int getValue(int r, int c) {
        return get(r, c).getValue();
    }

    public void set(int r, int c, int value) {
        get(r, c).setValue(value);
    }

    public void set(int r, int c, int value, boolean locked) {
        get(r, c).setLocked(locked);
        get(r, c).setValue(value);
    }

    /**
     * An empty cell, that is, 0, is by default true
     * @param row
     * @param col
     * @param value
     * @return
     */
    public boolean validValue(int row, int col, int value) {


        // first check if the value is actually valid
        if (value < 1 || value > 9) {
            return false;

        }

        // check if any same value in the same column
        for (int j = 0; j < Board.bsize; j++) {
            // don't check against itself
                if (cells[row][j].getValue() == value) return false;
        }

        // check if any same value in the same column
        for (int i = 0; i < Board.bsize; i++) {
                if (cells[i][col].getValue() == value) return false;
        }

        // check if any same value in the local 3x3 grid

        // determine local 3x3 grid
        int localRow = row / 3;
        int localCol = col / 3;
        for (int i = localRow * 3; i < localRow * 3 + 3; i++) {
            for (int j = localCol * 3; j < localCol * 3 + 3; j++) {
                    if (cells[i][j].getValue() == value) return false;
            }
        }
        return true;
    }

    public boolean validSolution() {
        for (int i = 0; i < Board.bsize; i++) {
            for (int j = 0; j < Board.bsize; j++) {
                if (cells[i][j].getValue() == 0 || !validValue(i, j, cells[i][j].getValue()) ) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * Draw function for LandscapeDisplay
     * @param g
     * @param gridScale
     */
    public void draw(Graphics g, int gridScale ) {
        // set alive color to black
        // draw all the cells
        for (int i = 0; i < Board.bsize; i++) {
            for (int j = 0; j < Board.bsize; j++) {
                if (j % 3 == 0 && j != 0) {
                    g.drawLine((j + 1) * gridScale - gridScale / 3, i * gridScale, (j + 1) * gridScale - gridScale / 3,
                            (i + 1) * gridScale);
                }
                cells[i][j].draw(g, j, i, gridScale);
            }
            if (i % 3 == 0 && i != Board.bsize * 2/3 ) {
                g.drawLine((1) * gridScale, (i + 3) * gridScale + gridScale / 3, (10) * gridScale, (i + 3) * gridScale + gridScale / 3);
            }
        }
    }

    public boolean isFilled(int r, int c) {
        return get(r, c).getValue() != 0;
    }


    public static void main(String[] args) {
        Board b = new Board();

        // test the board constructor
        System.out.println(b);

//        // Test the accessor functions
//        System.out.println(b.get(8, 8));
//        b.set(8, 8, 8);
//        System.out.println(b.get(8, 8));
//        System.out.println(b.get(0, 0));
//        b.set(0, 0, 7, true);
//        System.out.println(b.get(0, 0));
//        System.out.println(b.getCols());
//        System.out.println(b.getRows());
//        System.out.println(b.get(6, 7));


        // Test the betterRead function
        b.betterRead("./resources/board_nsp_10.txt");
        System.out.println(b);
        System.out.println(b.validSolution());

    }
}
