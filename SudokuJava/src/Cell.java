/**
 * Allen Ma
 * Feb 2018
 * Cell class in Sudoku
 */

import java.awt.*;

public class Cell {

    private int row;
    private int col;
    private int value;
    private boolean locked;

    public Cell() {
        row = 0;
        col = 0;
        value = 0;
        locked = false;
    }

    public Cell(int r, int c) {
        row = r;
        col = c;
        value = 0;
        locked = false;
    }

    public Cell(int r, int c, int v, boolean l) {
        row = r;
        col = c;
        value = v;
        locked = l;
    }

    public Cell(int r, int c, int v) {
        row = r;
        col = c;
        value = v;
        locked = false;
    }

    /**
     *
     * @return return the Cell's row index.
     */
    public int getRow() {
        return row;
    }

    /**
     *
     * @return return the Cell's column index.
     */
    public int getCol() {
        return col;
    }

    /**
     *
     * @return return the Cell's value.
     */
    public int getValue() {
        return value;
    }

    /**
     *
     * @param newval value
     */
    public void setValue(int newval) {
        value = newval;
    }

    /**
     *
     * @return return if the Cell is locked.
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     *
     * @param lock boolean whether to lock the cell or not
     */
    public void setLocked(boolean lock) {
        locked = lock;
    }

    /** Clone: return a new cell with the same values as the cell from which the
     * clone method was called in
     */
    public Cell clone() {
        return new Cell(this.row, this.col, this.value, this.locked);
    }

    public String toString() {
        return "Cell\n Row: " + this.row + "\n Column: " + this.col + "\n Value: " + this.value + "\n Locked: " + this.locked;
    }

    /**
     * draw method for cell
     * @param
     */
    public void draw(Graphics g, int x, int y, int scale) {
        char[] cellChar = {(char) ('0' + this.value)};
        g.drawChars(cellChar, 0, 1, (x + 1) *scale, (y + 1) *scale);
    }

    public static void main(String[] args) {
        // unit tests
        Cell cell1 = new Cell(6, 8, 8, false);
        System.out.println(cell1);

        Cell cell2 = cell1.clone();
        System.out.println(cell2);

        cell1.setLocked(true);
        System.out.println(cell1);

        cell1.setValue(9);
        System.out.println(cell1);

        cell2.setValue(1);
        System.out.println(cell2);
        System.out.println(cell1);
    }
}
