/**
 * Allen Ma
 * Feb 2018
 * Sudoku class
 */
import java.util.Collections;
import java.util.Random;
import java.util.ArrayList;

public class Sudoku {

    private Board b;
    private LandscapeDisplay display;

    /**
     * Constructor class
     */
    public Sudoku(LandscapeDisplay display, Board board) {
        b = board;
        this.display = display;
    }

    /**
     *
     * @param N - Represents the number of initially populated values in the board
     *         note that once you try generating over around 30 initial values, it starts taking
     *          way too long to generate the board.
     */
    public Sudoku(int N, LandscapeDisplay display, Board board) {
        Random r = new Random();
        b = board;
        // populate the board with random values

        // generate N unique values from 0 to 81
        ArrayList<Integer> list81 = new ArrayList<>();
        ArrayList<Integer> listNRow = new ArrayList<>();
        ArrayList<Integer> listNCol = new ArrayList<>();
        for (int i = 0; i < Board.bsize * Board.bsize - 1; i++) {
            list81.add(i);
        }
        Collections.shuffle(list81);
        for (int i = 0; i < N; i++) {
            int randRow = list81.get(i) % Board.bsize;
            int randCol = list81.get(i) / Board.bsize;
            listNRow.add(randRow);
            listNCol.add(randCol);
        }

        for (int i = 0; i < N; i++) {
            // generate a random value to fill in
            int randVal = r.nextInt(Board.bsize) + 1;
            while (!b.validValue(listNRow.get(i), listNCol.get(i), randVal)) {
                randVal = r.nextInt(Board.bsize) + 1;
            }
            b.set(listNRow.get(i), listNCol.get(i), randVal, true);
        }

        this.display = display;
    }

    public Board getBoard() {
        return b;
    }

    /**
     * Recursive backtracking solution
     * Using a stack
     */
    public boolean solve(int delay) {

        // in principle, a solved sudoku will have 81 values on the stack
        // if there are zero values on the stack, then that means the sudoku is unsolvable

        // declare a CellStack variable and assign it a new CellStack of size 100
        CellStack cellStack = new CellStack(100);

        // declare int variables curRow, curCol, curValue, and time.
        int curRow = 0;
        int curCol = 0;
        int time = 0;
        int curValue = 1;
        // set curRow, curCol, and time to zero; set curValue to 1

        // while the size of the stack is less than Board.Size * Board.Size
        while (cellStack.size() < Board.bsize * Board.bsize) {
            time++;

            // delay code for visualization
            if( delay > 0 ) {
                try {
                    Thread.sleep(delay);
                }
                catch(InterruptedException ex) {
                    System.out.println("Interrupted");
                }
                display.repaint();
            }

            // if a cell is locked, then move onto the next cell
            if (b.isLocked(curRow, curCol)) {
                cellStack.push(b.get(curRow, curCol));
                // prepare next indices
                curCol++;
                if (curCol % Board.bsize == 0) {
                    curCol = 0;
                    curRow++;
                }
                continue;
            }

            for (int i = curValue; i <= 9; i++) {
                if (b.validValue(curRow, curCol, i)) {
                    curValue = i;
                    break;
                }
            }

            if (b.validValue(curRow, curCol, curValue)) {
                Cell validCell = new Cell(curRow, curCol, curValue);
                cellStack.push(validCell);


                // set the value on the board
                b.set(curRow, curCol, curValue);

                curCol++;
                if (curCol % Board.bsize == 0) {
                    curCol = 0;
                    curRow++;
                }
                // reset curVal for next cell
                curValue = 1;
            }
            else {
                if (cellStack.size() > 0) {
                    //backtrack
                    Cell topCell = cellStack.pop();
                    while (topCell.isLocked()) {
                        if (cellStack.size() > 0) {
                            topCell = cellStack.pop();
                        }
                        else {
                            System.out.print("Number of steps " + time);
                            // no solution found :<
                            return false;
                        }
                    }
                    curRow = topCell.getRow();
                    curCol = topCell.getCol();
                    curValue = topCell.getValue() + 1;

                    b.set(curRow, curCol, 0);

                }
                else {
                    System.out.print("Number of steps " + time);
                    // no solution found :<
                    return false;
                }
            }
        }
        System.out.println("Solved!");
        System.out.print("Number of steps " + time);
        return true;
    }

    public static void main(String[] args) {
//        if (args.length > 0) {
//            Sudoku s = new Sudoku(Integer.parseInt(args[0]));
//            System.out.println("Initial state of board");
//            System.out.println(s.getBoard());
//            System.out.println("Solving...");
//            // 10 ms delay
//            s.solve(10);
//            System.out.println(s.getBoard());
//        }
//        else {
//            Sudoku s = new Sudoku();
//            System.out.println("Initial state of board");
//            System.out.println(s.getBoard());
//            System.out.println("Solving...");
//            // 10 ms delay
//            s.solve(10);
//            System.out.println(s.getBoard());
//        }
    }
}
