/**
 * Allen Ma
 * Feb 2018
 * CellStack class
 */


import java.util.EmptyStackException;

public class CellStack {

    private Cell[] cellarr;
    private int max_size;
    private int top;

    public CellStack() {
        max_size = 10;
        top = 0;
        cellarr = new Cell[10];
    }

    public CellStack(int sz) {
        max_size = sz;
        top = 0;
        cellarr = new Cell[sz];
    }

    /**
     * Pushes cell c onto the stack
     * Resizes if necessary by doubling the array
     * @param c
     */
    public void push(Cell c) {
        // there is still space
        if (top < max_size) {
            cellarr[top++] = c;
        }
        else {
            resize(max_size * 2);
            // add the element
            cellarr[top++] = c;
        }
    }

    /**
     * Pop method for the stack
     * @return the top most element on the stack
     */
    public Cell pop() {
        // add resizing capability
        // resize when the cap
        if (top != 0) {
            return cellarr[--top];
        }
        return null;
    }

    /**
     * Get the number of elements on the stack
     * @return the size - number of elements on the stack
     */
    public int size() {
        // assert (top == cellarr.length);
        return top;
    }

    /**
     *
     * @return boolean indicating whether the stack is empty or not
     */
    public boolean empty() {
        return top == 0;
    }

    /**
     * Resize method - allocates a new array the size of capacity and copies everything over
     * @param capacity
     */
    private void resize(int capacity) {
        Cell[] temp = new Cell[capacity];
        for (int i = 0; i < max_size; i++) {
            temp[i] = cellarr[i];
        }
        max_size = capacity;
        cellarr = temp;
    }

    public static void main(String[] args) {
        // unit tests
        CellStack test = new CellStack();
        System.out.println("The number of elements on the stack is " + test.size());
        test.push(new Cell());
        System.out.println("The number of elements on the stack is " + test.size());
        System.out.println(test.pop());
        System.out.println("The number of elements on the stack is " + test.size());
        test.push(new Cell());
        System.out.println("The number of elements on the stack is " + test.size());
        test.pop();
        System.out.println("The number of elements on the stack is " + test.size());

        for (int i = 0; i < 20984; i++) {
            test.push(new Cell(i, 1));
        }
        test.pop();
        System.out.println("The number of elements on the stack is " + test.size());

    }
}
