public abstract class Simulation {

    private LandscapeDisplay display;
    private Landscape simScape;

    public abstract Landscape getLandscape();
    public abstract LandscapeDisplay getDisplay();

}