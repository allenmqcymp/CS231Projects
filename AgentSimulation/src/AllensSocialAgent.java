import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.Random;

/**
 * Allen Ma
 * March 2018
 */
public class AllensSocialAgent extends SocialAgent {


    public AllensSocialAgent(double x0, double y0) {
        super( x0, y0 );
    }

    @Override
    public void draw(Graphics g)  {
        // cast graphics into graphics 2d to draw a 2.5 radius circle
        Graphics2D g2 = (Graphics2D) g;

        /* Enable anti-aliasing and pure stroke */
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        /* Construct a shape and draw it */
        Ellipse2D.Double shape = new Ellipse2D.Double(x, y,  2.5, 2.5);
        g2.draw(shape);

        g2.fill(shape);

    }

    public void updateState(Landscape scape) {
        //        Rule
        //        If the cell has more than 3 neighbors (Agents besides itself) within a
        //        radius of 15, then with a 1% chance, the cell should move randomly
        //        within the range [-5,5] else the cell should move randomly within
        //        the range [-5, 5]
        Random chanceGen = new Random();
        Random moveGen = new Random();
        double radius = 25.0;
        if (scape.getNeighbors(x, y, radius).size() > 3) {
            // within a %1 chance
            // generate in between -5 and 5
            double randomX = -2 + 4 * moveGen.nextDouble();
            double randomY = -2 + 4 * moveGen.nextDouble();
            x = (x  + randomX);
            y = (y + randomY);
        }
        else {
            if (chanceGen.nextDouble() < 0.01) {
                double randomX = -2 + 4 * moveGen.nextDouble();
                double randomY = -2 + 4 * moveGen.nextDouble();
                x = (x + randomX);
                y = (y + randomY);
            }

        }
    }



}
