/**
 * Allen Ma
 * March 2018
 */
public class OneSimulation {

    private Simulation simulation;

    public OneSimulation() {
        System.out.print("No type provided");
    }

    public OneSimulation(int kind) {
        if (kind == 1) {
            simulation = new SocialAgentSimulation(500, 500, 200);
        }
        else if (kind == 2) {
            simulation = new CategorizedSocialAgentSimulation(500, 500, 200, 2);
        }
        else if (kind == 3) {
            simulation = new AllenSocialAgentSimulation(500, 500, 200);
        }
    }

    public Simulation getSimulation() {
        return simulation;
    }

    public static void main(String[] args) throws InterruptedException, IllegalArgumentException {

        OneSimulation os;

        if (args.length == 1) {
            String type = args[0];

            if (type.equalsIgnoreCase("socialagent")) {
                os = new OneSimulation(1);
            }
            else if (type.equalsIgnoreCase("categorysocialagent")) {
                os = new OneSimulation(2);
            }
            else if (type.equalsIgnoreCase("allenssocialagent")) {
                os = new OneSimulation(3);
            }
            else {
                throw new IllegalArgumentException("not recognized argument");
            }
        }
        else {
            os = new OneSimulation(1);
        }

        while (true) {
            if (!os.getSimulation().getDisplay().isPaused()) {
                os.getSimulation().getLandscape().updateAgents();
                os.getSimulation().getDisplay().repaint();
            }
            else {
                os.getSimulation().getDisplay().repaint();
            }
        }
    }
}
