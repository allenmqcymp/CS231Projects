/**
 * Allen Ma
 * March 2018
 */
import java.awt.Graphics;
public abstract class Agent {

    protected double x;
    protected double y;

    /**
     * Sets x and y positions of the agent object
     * @param x0
     * @param y0
     */
    public Agent(double x0, double y0){
        x = x0;
        y = y0;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public void setX( double newX ) {
        x = newX;
    }
    public void setY( double newY ) {
        y = newY;
    }
    public String toString() {
        return "Position. X: " + x + " Y: " + y;
    }
    public void updateState( Landscape scape ) {

    }
    public abstract void draw(Graphics g) ;

    public int getCategory() throws RuntimeException {
        throw new RuntimeException();
    }

    /**
     * Tests
     * @param args
     */
    public static void main(String[] args) {
        /*Agent agent1 = new Agent(0.0, 0.0);
        Agent agent2 = new Agent(1.0, 10.0);

        System.out.println(agent1);
        System.out.println(agent2);

        agent1.setX(-50.2);
        agent2.setY(-34.6);

        System.out.println(agent1);
        System.out.println(agent2);*/


    }
}
