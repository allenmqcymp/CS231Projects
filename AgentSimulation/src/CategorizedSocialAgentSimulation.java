/**
 * Allen Ma
 * March 2018
 */

import java.util.Random;

public class CategorizedSocialAgentSimulation extends Simulation {

    protected LandscapeDisplay display;
    protected Landscape simScape;

    public LandscapeDisplay getDisplay() {
        return display;
    }

    public Landscape getLandscape() {
        return simScape;
    }

    public CategorizedSocialAgentSimulation(int w, int h, int N, int num_categories) {
        simScape = new Landscape(w, h);
        Random gen = new Random();

        // add two categories of things
        for (int i = 0; i < N / num_categories; i++) {
            simScape.addAgent(new CategorizedSocialAgent(gen.nextDouble() * simScape.getWidth(),
                    gen.nextDouble() * simScape.getHeight(), 1, 15));
        }

        for (int j = 0; j < N / num_categories; j++) {
            simScape.addAgent(new CategorizedSocialAgent(gen.nextDouble() * simScape.getWidth(),
                    gen.nextDouble() * simScape.getHeight(), 2, 15));
        }

        for (int k = 0; k < N / num_categories; k++) {
            simScape.addAgent(new CategorizedSocialAgent(gen.nextDouble() * simScape.getWidth(),
                    gen.nextDouble() * simScape.getHeight(), 3, 15));
        }

        display = new LandscapeDisplay(simScape);

        display.repaint();
    }

//    public static void main(String[] args) throws InterruptedException {
//        CategorizedSocialAgentSimulation sas = new CategorizedSocialAgentSimulation(500, 500, 100, 100);
//
//        while (true) {
//            // sleep
//            Thread.sleep(100);
//            sas.getLandscape().updateAgents();
//
//            sas.getDisplay().repaint();
//        }
//    }

}