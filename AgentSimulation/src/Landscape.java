/**
 * Allen Ma
 * March 2018
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.awt.Graphics;

public class Landscape {

    private int height;
    private int width;

    LinkedList<Agent> agentList = new LinkedList<>();

    /**
     * a constructor that sets the width and height fields, and initializes the agent list.
     *
     * @param w
     * @param h
     */
    public Landscape(int w, int h) {
        height =h;
        width = w;
    }

    /**
     * returns the height.
     *
     * @return
     */
    public int getHeight() {
        return height;
    }

    /**
     * returns the width.
     *
     * @return
     */
    public int getWidth() {
        return width;
    }

    /**
     * inserts an agent at the beginning of its list of agents.
     *
     * @param a
     */
    public void addAgent(Agent a) {
        agentList.addFirst(a);
    }

    public void removeAgent(int i) {
        agentList.remove(i);
    }

    /**
     * returns a String representing the Landscape. It can be as simple as indicating the number of Agents on the Landscape.
     *
     * @return
     */
    public String toString() {
        return "number of agents on landscape: " + agentList.size();
    }

    /**
     * returns a list of the Agents within radius distance of the location x0, y0.
     *
     * @param x0
     * @param y0
     * @param radius
     * @return
     */
    public ArrayList<Agent> getNeighbors(double x0, double y0, double radius) {

        ArrayList<Agent> neighbors = new ArrayList<>();
        for (Agent a: agentList) {
            // calculate distance
            double x = x0 - a.getX();
            double y = y0 - a.getY();

            if (Math.sqrt(x * x + y * y) <= radius) {
                neighbors.add(a);
            }
        }
        return neighbors;
    }

    /**
     * Draws stuff on the graphics object
     *
     * @param g
     */
    public void draw(Graphics g) {
        agentList.forEach(agent ->{
            agent.draw(g);
        });

    }

    public void updateAgents() {
        // shuffle the list
        ArrayList<Agent> agentArrayList = agentList.toShuffledList();
        for (Agent a: agentArrayList) {
            a.updateState(this);
        }

    }
//
//    public static void main(String[] args) {
//       Landscape scapeTest = new Landscape(500, 500);
//
//       SocialAgent agent1 = new SocialAgent(5, 5);
//       SocialAgent agent2 = new SocialAgent(10, 10);
//       SocialAgent agent3 = new SocialAgent(15, 15);
//       SocialAgent agent4 = new SocialAgent(19, 20);
//
//       scapeTest.addAgent(agent1);
//       scapeTest.addAgent(agent2);
//       scapeTest.addAgent(agent3);
//       scapeTest.addAgent(agent4);
//
//       System.out.println(scapeTest.getNeighbors(agent1.getX(), agent1.getY(), 20.0));
//
//
//    }

}