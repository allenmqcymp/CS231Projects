/*
	Originally written by Bruce A. Maxwell a long time ago.
	Updated by Brian Eastwood and Stephanie Taylor more recently

	Creates a window using the JFrame class.

	Creates a drawable area in the window using the JPanel class.

	The JPanel calls the Landscape's draw method to fill in content, so the
	Landscape class needs a draw method.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Displays a Board graphically using Swing.  The Board can be
 * displayed at any scale factor.  The only change from project 2
 * is the use of the Board class instead of the Landscape class.
 * @author bseastwo
 */
public class LandscapeDisplay extends JFrame implements ActionListener
{
    protected Landscape scape;
    private LandscapePanel canvas;
    private JButton pause;

    private boolean paused;

    /**
     * Initializes a display window for a Landscape.
     * There is no scale in this case
     * @param scape the Landscape to display
     */
    public LandscapeDisplay(Landscape scape)
    {
        // setup the window
        super("Agents");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.scape = scape;

        // create a panel in which to display the Landscape
        this.canvas = new LandscapePanel( this.scape.getWidth(),
																					this.scape.getHeight() );

        // create a button
        this.pause = new JButton("toggle pause/play");

        // add action listener
        this.pause.addActionListener(this);

        // set paused to false
        paused = false;


        // add the panel to the window, layout, and display
        this.add(this.canvas, BorderLayout.CENTER);

        // add button to canvas
        this.canvas.add(this.pause, BorderLayout.NORTH);

        this.pack();
        this.setVisible(true);
    }

    public boolean isPaused() {
        return paused;
    }

    public void actionPerformed(ActionEvent e) {
        // code that reacts to the action
        // was going to use:
        paused = !paused;
    }



    /**
     * This inner class provides the panel on which Landscape elements
     * are drawn.
     */
    private class LandscapePanel extends JPanel
    {
        /**
         * Creates the panel.
         * @param width     the width of the panel in pixels
         * @param height        the height of the panel in pixels
         */
        public LandscapePanel(int width, int height)
        {
                super();
                this.setPreferredSize(new Dimension(width, height));
                this.setBackground(Color.white);
        }

        /**
         * Method overridden from JComponent that is responsible for
         * drawing components on the screen.  The supplied Graphics
         * object is used to draw.
         *
         * @param g     the Graphics object used for drawing
         */
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            scape.draw( g );
        } // end paintComponent

    } // end LandscapePanel

    public void update() {
        Graphics g = canvas.getGraphics();
        this.requestFocus();
        canvas.paintComponent( g );
    }


//		// test function that creates a new LandscapeDisplay and populates it with 200 agents.
//    public static void main(String[] args) throws InterruptedException {
//        Landscape scape = new Landscape(500, 500);
//				Random gen = new Random();
//
//				for(int i=0;i<200;i++) {
//						scape.addAgent( new SocialAgent( gen.nextDouble() * scape.getWidth(),
//																						 gen.nextDouble() * scape.getHeight() ) );
//				}
//
//        LandscapeDisplay display = new LandscapeDisplay(scape);
//
//        display.repaint();
//		}
}
