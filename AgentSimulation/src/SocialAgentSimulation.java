/**
 * Allen Ma
 * March 2018
 */

import java.util.Random;

public class SocialAgentSimulation extends Simulation {

    protected LandscapeDisplay display;
    protected Landscape simScape;

    public LandscapeDisplay getDisplay() {
        return display;
    }

    public Landscape getLandscape() {
        return simScape;
    }

    public SocialAgentSimulation(int w, int h, int N) {
        simScape = new Landscape(w, h);
        Random gen = new Random();

        for (int i = 0; i < N; i++) {
            simScape.addAgent(new SocialAgent(gen.nextDouble() * simScape.getWidth(),
                    gen.nextDouble() * simScape.getHeight()));
        }

        display = new LandscapeDisplay(simScape);

        display.repaint();
    }

//    public static void main(String[] args) throws InterruptedException {
//        SocialAgentSimulation sas = new SocialAgentSimulation(500, 500, 200);
//
//        while (true) {
//            // sleep
//            Thread.sleep(100);
//            sas.getLandscape().updateAgents();
//
//            sas.getDisplay().repaint();
//        }
//    }



}