import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;

public class LinkedList<T> implements Iterable<T> {

    private int size;
    private Node head;


    public LinkedList() {
        head = null;
        size = 0;
    }
    public void clear() {
        head = null;
        size = 0;
    }
    public int size() {
        return size;
    }
    public void addFirst(T item) {
        if (size() == 0) {
            head = new Node(item);
            head.next = null;
        }
        else {
            Node newNode = new Node(item);
            newNode.next = head;
            head = newNode;
        }
        size++;
    }

    public void addLast(T item) {
        if (size() == 0) {
            head = new Node(item);
            head.next = null;
            size++;
            return;
        }
        Node n = head;
        while (n.next != null) {
            n = n.next;
        }
        // last element of list
        Node newItem = new Node(item);
        n.next = newItem;
        newItem.next = null;
        size++;
    }

    /**
     * Add 0 means add at 0th position
     * Add 4 means make the added element be in 4th position at the end of the add
     * @param index
     * @param item
     */
    public void add(int index, T item) {
        if (index < 0) {
            throw new IllegalArgumentException("Illegal index");
        }
        else if (index > size()) {
            throw new IllegalArgumentException("Index must be in range 0 to n-1, where n-1 is the index of the last elem");
        }
        if (size() == 0) {
            if (index != 0) {
                throw new IllegalArgumentException("When adding to an empty, list, index must be 0");
            }
            head = new Node(item);
            head.next = null;
            size++;
            return;
        }

        // we have to adjust head if index is 0
        if (index == 0) {
            addFirst(item);
        }
        else {
            Node n = head;
            int count = 1;
            while (count < index) {
                count++;
                n = n.next;
            }
            Node addNode = new Node(item);
            if (n.next == null) {
                addNode.next = null;
            }
            else {
                addNode.next = n.next;
            }
            n.next = addNode;
            size++;
        }
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> arrl = new ArrayList<T>();
        Iterator<T> it = iterator();
        while (it.hasNext()) {
            T t = it.next();
            arrl.add(t);
        }
        return arrl;
    }

    public ArrayList<T> toShuffledList() {
        ArrayList<T> arrl = toArrayList();
        Collections.shuffle(arrl);
        return arrl;
    }


    public T remove(int index) {
        if (index < 0 || index > size() - 1) {
            throw new IllegalArgumentException("Cannot remove from invalid index");
        }
        if (index == 0) {
            Node retNode = head;
            head = head.next;
            size--;
            return retNode.item;
        }
        else {
            int count = 0;
            Node n = head;
            while (count != index - 1) {
                n = n.next;
                count++;
            }
            Node before = n;
            Node retNode = before.next;
            if (retNode.next == null) {
                before.next = null;
            }
            else {
                before.next = retNode.next;
            }
            size--;
            return retNode.item;
        }
    }

    public LLIterator iterator() {
        return new LLIterator(head);
    }


    private class Node {

        private T item;
        private Node next;

        public Node(T item) {
            this.item = item;
            this.next = null;
        }
        public T getThing() {
            return item;
        }
        public void setNext(Node n) {
            next = n;
        }
        public Node getNext() {
            return next;
        }
    }

    private class LLIterator implements Iterator<T> {

        private Node currentNode;

        public LLIterator(Node head) {
            currentNode = head;
        }

        public boolean hasNext() {
            return currentNode != null;
        }

        public T next() {
            Node retNode = currentNode;
            currentNode = currentNode.next;
            return retNode.getThing();
        }
        // does nothing
        public void remove() {

        }
    }
}
