/**
 * Allen Ma
 * March 2018
 */

import java.awt.*;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.Random;


public class CategorizedSocialAgent extends SocialAgent {

    private int category;
    private int radius;
    public CategorizedSocialAgent(double x0, double y0, int cat, int r) {
        super(x0, y0);
        category = cat;
        radius = r;
    }

    @Override
    public void draw(Graphics g)  {
        // cast graphics into graphics 2d to draw a 2.5 radius circle
        Graphics2D g2 = (Graphics2D) g;

        /* Enable anti-aliasing and pure stroke */
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);


        /* Construct a shape and draw it */
        Ellipse2D.Double shape = new Ellipse2D.Double(x, y,  2.5, 2.5);
        if (category == 1) {
            g2.setColor(Color.RED);
        }
        else if (category == 2) {
            g2.setColor(Color.BLUE);
        }
        else if (category == 3) {
            g2.setColor(Color.GREEN);
        }
        g2.draw(shape);

    }

//    public void updateState(Landscape scape) {
////        If there are more of the same category within a radius of 20
////          with a 1% chance move randomly within the range [-5, 5] in x and y.
////        else
////          move randomly within the range [-5, 5] in x and y.
//
//        Random chanceGen = new Random();
//        Random moveGen = new Random();
//
//        int same_count = 0;
//        int count = 0;
//        for (Agent a: scape.getNeighbors(x, y, radius)) {
//            // comparison of ints so == used
//
//            if (a.getCategory() == getCategory()) {
//                count++;
//                same_count++;
//            }
//            else {
//                count++;
//            }
//        }
//
//        if (same_count > count - same_count) {
//            // within a %1 chance
//            if (chanceGen.nextDouble() <= 0.01) {
//                // generate in between -5 and 5
//                double randomX = -5.0 + 10 * moveGen.nextDouble();
//                double randomY = -5.0 + 10 * moveGen.nextDouble();
//                x = (x  + randomX);
//                y = (y + randomY);
//            }
//        }
//        // definitely move
//        else {
//            double randomX = -5.0 + 10 * moveGen.nextDouble();
//            double randomY = -5.0 + 10 * moveGen.nextDouble();
//            x = (x + randomX);
//            y = (y + randomY);
//        }
//
//    }

    /**
     * Adjusts x and y by -range /2 to range / 2 randomly
     */
    public void move(int range) {
        Random moveGen = new Random();
        double randomX = -range + 2 * range * moveGen.nextDouble();
        double randomY = -range + 2 * range * moveGen.nextDouble();
        x = (x  + randomX);
        y = (y + randomY);
    }

    public void  updateState(Landscape scape) {

        // Rules - reds hate
        Random chanceGen = new Random();


        int blue_count = 0;
        int red_counter = 0;
        int count = 0;
        int green_counter = 0;

        for (Agent a: scape.getNeighbors(x, y, radius)) {
            // comparison of ints so == used

            if (getCategory() == 3) {
                // green
                if (a.getCategory() == getCategory()) {
                    green_counter++;
                }
            }
            else if (getCategory() == 2) {
                // blues
                if (a.getCategory() == getCategory()) {
                    blue_count++;
                }

                else if (a.getCategory() == 1) {
                    red_counter++;
                }
            }
            else if (getCategory() == 1) {
                if (a.getCategory() == getCategory()) {
                    red_counter++;
                }
                else if (a.getCategory() == 2) {
                    blue_count++;
                }

            }
            count++;
        }

        if (getCategory() == 3) {
            if (green_counter >= count - green_counter) {
                if (chanceGen.nextDouble() < 0.01) {
                    move(100);
                }
            }
        }
        else if (getCategory() == 2) {
            if (red_counter > 3) {
                move(10);
            }
            else if (blue_count >= count - blue_count) {
                if (chanceGen.nextDouble() < 0.01) {
                    move(5);
                }
            }
            else {
                if (chanceGen.nextDouble() < 0.1) {
                    move(5);
                }
            }

        }
        else {
            if (blue_count > 3) {
                move(10);
            }
            else if (red_counter >= count - red_counter) {
                if (chanceGen.nextDouble() < 0.01) {
                    move(5);
                }
            }
            else {
                if (chanceGen.nextDouble() < 0.1) {
                    move(5);
                }
            }
        }

    }

    public int getCategory() {
        return category;
    }

    public String toString() {
        return Integer.toString(category);
    }

}


