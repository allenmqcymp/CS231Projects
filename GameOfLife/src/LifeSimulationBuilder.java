
public class LifeSimulationBuilder {

    // defaults
    private int _scale = 20;
    private int _rows = 10;
    private int _columns = 10;
    private double _density = 0.3;
    private int _iterations = -1;
    private int _speed = 100;

    public LifeSimulationBuilder() {

    }

    public LifeSimulationBuilder iterations(int i) {
        this._iterations = i;
        return this;
    }

    public LifeSimulationBuilder speed(int s) {
        this._speed = s;
        return this;
    }

    public LifeSimulation build() {
        return new LifeSimulation(_rows, _columns, _density, _scale, _iterations, _speed);
    }

    public LifeSimulationBuilder rows(int rows) {
        this._rows = rows;
        return this;
    }

    public LifeSimulationBuilder columns(int cols) {
        this._columns = cols;
        return this;
    }

    public LifeSimulationBuilder scale(int scale) {
        this._scale = scale;
        return this;
    }

    public LifeSimulationBuilder density(double density) {
        this._density = density;
        return this;
    }
}
