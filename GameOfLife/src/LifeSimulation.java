import com.sun.tools.corba.se.idl.InterfaceGen;
import com.sun.tools.corba.se.idl.ParameterEntry;

import java.util.Random;

/**
 * Allen Ma
 * Feb 2018
 * LifeSimulation client class
 */

public class LifeSimulation {

    private int rows;
    private int cols;
    private double density;
    private int scale;
    private long iterations;
    private int speed;

    private Landscape scape;
    private LandscapeDisplay display;

    public LifeSimulation(int rows, int cols, double density, int scale, long iterations, int speed) {
        this.rows = rows;
        this.cols = cols;
        this.density = density;
        this.scale = scale;
        this.iterations = iterations;
        this.speed = speed;

        this.scape = new Landscape(this.rows, this.cols);
        Random gen = new Random();

        for (int i = 0; i < scape.getRows(); i++) {
            for (int j = 0; j < scape.getCols(); j++ ) {
                scape.getCell( i, j ).setAlive( gen.nextDouble() <= density );
            }
        }

        this.display = new LandscapeDisplay(scape, scale);

    }

    public long getIterations() {
        return iterations;
    }

    public int getSpeed() {
        return speed;
    }

    public Landscape getScape() {
        return scape;
    }

    public LandscapeDisplay getDisplay() {
        return display;
    }



    public static void main(String[] args) throws InterruptedException {

        // Command line arguments
        // rows, cols, density, scale
        LifeSimulation sim;
        // create the life simulation object using a builder design pattern
        if (args.length == 6) {
            sim = new LifeSimulationBuilder()
                    .rows(Integer.parseInt(args[0]))
                    .columns(Integer.parseInt(args[1]))
                    .density(Double.parseDouble(args[2]))
                    .scale(Integer.parseInt(args[3]))
                    .iterations(Integer.parseInt(args[4]))
                    .speed(Integer.parseInt(args[5]))
                    .build();
        }
        else {
            sim = new LifeSimulationBuilder().build();
        }

        sim.getDisplay().repaint();

        int count = 0;
        while (count != sim.getIterations()) {
            sim.getScape().advance();
            Thread.sleep(sim.getSpeed());
            sim.getDisplay().repaint();
            count++;
        }

        System.out.println("Iterations reached:" + sim.getIterations());

    }

}