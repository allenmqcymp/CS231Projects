/**
 * Allen Ma
 * Feb 2018
 * CS 231 Landscape Class
 */

import java.util.Arrays;
import java.util.ArrayList;
import java.awt.Color;
import java.lang.ArrayIndexOutOfBoundsException;
import java.lang.StringBuilder;
import java.awt.Graphics;

public class Landscape {

    private Cell[][] grid;

    /**
     * Constructor for Landscape class
     */
    public Landscape( int rows, int cols ) {
        grid = new Cell[rows][cols];

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                grid[i][j] = new Cell(false);
            }
        }
    }

    // cloning constructor
    public Landscape(Landscape l) {
        this.grid = l.grid;
    }

    /**
     * Reset each cell's state in the grid
     */
    public void reset() {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                grid[i][j].setAlive(false);
            }
        }
    }

    /**
     * Return number of rows for grid
     */
    public int getRows() {
        return grid.length;
    }

    /**
     * Return number of columns for grid
     */
    public int getCols() {
        return grid[0].length;
    }

    /*
     * Return the cell at address row, col
     */
    public Cell getCell( int row, int col ) {
        if (!isLegal(row, col) ) {
            throw new ArrayIndexOutOfBoundsException("Illegal input for getCell");
        }
        else {
            return grid[row][col];
        }
    }

    /*
     * Return an arraylist of cells
     * Representing neighbors of cell at row, col
     */
    public ArrayList<Cell> getNeighbors( int row, int col ) {

        if (!isLegal(row, col) ) {
            throw new ArrayIndexOutOfBoundsException("Illegal input for getCell");
        }

        ArrayList<Cell> neighbors = new ArrayList<>();
        /*
            :)

            |_|_|_|
            |_|X|_|
            |_|_|_|
        */

        // top left
        if (isLegal(row - 1, col - 1)) {
            neighbors.add(getCell(row-1,col-1));
        }

        // top
        if (isLegal(row - 1, col)) {
            neighbors.add(getCell(row-1, col));
        }

        // top right
        if (isLegal(row - 1, col + 1)) {
            neighbors.add(getCell(row - 1, col + 1));
        }

        // right
        if (isLegal(row, col + 1)) {
            neighbors.add(getCell(row, col + 1));
        }

        // bottom right
        if (isLegal(row + 1, col + 1)) {
            neighbors.add(getCell(row + 1, col + 1));
        }

        // bottom
        if (isLegal(row + 1, col)) {
            neighbors.add(getCell(row + 1, col));
        }

        // bottom left
        if (isLegal(row + 1, col - 1)) {
            neighbors.add(getCell(row + 1, col - 1));
        }

        // left
        if (isLegal(row, col - 1)) {
            neighbors.add(getCell(row, col - 1));
        }
        return neighbors;
    }

    /**
     * String representation of the landscape
     * If a cell is dead, then it is a space character
     * Otherwise, it is an 'X'
     *
     * Using a stringbuilder class because string concatenation is quadratic
     * time
     */
    public String toString() {
        StringBuilder stateString = new StringBuilder();
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                if (!grid[i][j].getAlive()) {
                    stateString.append(" ");
                }
                else {
                    stateString.append("X");
                }
            }
            stateString.append("\n");
        }
        stateString.append("\n");
        return stateString.toString();
    }

    /**
     * Tests if you provide a legal array address
     * for an array of 2 x 2, array[0][2] is legal for example
     * 2 does not exist (since arrays are 0-indexed)
     */
    private boolean isLegal(int row, int col) {
        if (row < 0 || row > grid.length - 1) {
            return false;
        }
        if (col < 0 || col > grid[0].length - 1) {
            return false;
        }
        return true;
    }

    public void draw( Graphics g, int gridScale ) {
        // set alive color to black
        // draw all the cells
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getCols(); j++) {
                this.grid[i][j].draw(g, i*gridScale, j*gridScale, gridScale);
            }
        }
    }

    public void advance() {
        // create a temporary landscape object
        Landscape temp = new Landscape(this);

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                temp.getCell(i, j).updateState(this.getNeighbors(i, j));
            }
        }
        this.grid = temp.grid;
    }

}