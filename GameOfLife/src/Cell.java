import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Allen Ma
 * Feb 2018
 * Cell class
 */

public class Cell {

    private Random gen = new Random();

    private boolean alive;

    /**
     * Constructor for cell class
     * Default alive is false
     */
    public Cell() {
        alive = false;
    }

    /**
     * Constructor for cell class that takes in a boolean value
     * Indicating alive or dead
     */
    public Cell( boolean alive ) {
        this.alive = alive;
    }

    /**
     * Returns alive or dead (true for alive)
     */
    public boolean getAlive( ) {
        return alive;
    }

    /**
     * Sets alive value (boolean)
     */
    public void setAlive( boolean alive ) {
        this.alive = alive;
    }

    /**
     * Return 1 for alive; 0 for dead;
     */
    @Override
    public String toString() {
        return alive ? "1" : "0";
    }

    public static void main(String[] args) {
        Cell c = new Cell(true);

        System.out.println(c.toString());
    }

    /**
     * Draw method for graphics
     */
    public void draw(Graphics g, int x, int y, int scale) {
        // if alive, draw a black square
        // otherwise, draw nothing]
        if (getAlive()) {
            g.setColor( new Color(
                    (gen.nextInt(255) + 255) / 2,
                    (gen.nextInt(255) + 255) / 2,
                    (gen.nextInt(255) + 255) / 2
            ));

            g.fillRoundRect(x, y, scale, scale, scale/2, scale/2);
        }
        else {
            g.setColor(new Color(0, 0, 0));
            g.drawRoundRect(x, y, scale, scale, scale/2, scale/2);
        }

    }

    public void updateState( ArrayList<Cell> neighbors ) {
        int alive = 0;
        for (Cell n: neighbors) {
            if (n.getAlive()) { alive++; }
        }

        // alive cells
        if (this.getAlive()) {
            if (alive == 2 || alive == 3) {
                this.setAlive(true);
            }
            else {
                this.setAlive(false);
            }
        }
        else {
            if (alive == 3) {
                this.setAlive(true);
            }
            else {
                this.setAlive(false);
            }
        }

    }

}